import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/models/repair_station_model.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/repositories/repair_stations_repository.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/usecases/repair_stations_usecase.dart';

import 'get_repair_stations_usecase_test.mocks.dart';

@GenerateMocks([GetRepairStationsRepository])
void main() {
  late MockGetRepairStationsRepository getRepairStationsRepositoryMock;
  late GetRepairStationsUseCase getRepairStationsUseCase;
  final dummyResponse = [RepairStationModel(), RepairStationModel()];
  setUp(() {
    getRepairStationsRepositoryMock = MockGetRepairStationsRepository();
    getRepairStationsUseCase = GetRepairStationsUseCase(getRepairStationsRepositoryMock);
  });

  test(' Repair stations usecase test', () async {
    //set-up
    when(getRepairStationsRepositoryMock.getRepairStations())
        .thenAnswer((_) async => Right(dummyResponse));
    //act
    await getRepairStationsUseCase.call();
    //verify
    verify(getRepairStationsRepositoryMock.getRepairStations());
  });
}
