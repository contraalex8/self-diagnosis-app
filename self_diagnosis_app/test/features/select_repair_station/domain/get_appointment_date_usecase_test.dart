import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/repositories/get_appointment_date_repository.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/usecases/get_appointment_date_usecase.dart';

import 'get_appointment_date_usecase_test.mocks.dart';

@GenerateMocks([GetAppointmentDateRepository])
void main() {
  late MockGetAppointmentDateRepository getAppointmentDateRepositoryMock;
  late GetAppointmentDateUseCase getAppointmentDateUseCase;
  final dummyResponse = DateAppointmentModel();
  setUp(() {
    getAppointmentDateRepositoryMock = MockGetAppointmentDateRepository();
    getAppointmentDateUseCase = GetAppointmentDateUseCase(getAppointmentDateRepositoryMock);
  });

  test('Get appointment date data usecase test', () async {
    //set-up
    when(getAppointmentDateRepositoryMock.getAppointmentDate())
        .thenAnswer((_) async => Right(dummyResponse));
    //act
    await getAppointmentDateUseCase.call();
    //verify
    verify(getAppointmentDateRepositoryMock.getAppointmentDate());
  });
}
