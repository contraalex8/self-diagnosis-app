import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/usecases/get_appointment_date_usecase.dart';
import 'package:self_diagnosis_app/features/select_repair_station/presentation/bloc/get_appointment_date_bloc/get_appointment_date_bloc.dart';

import 'get_appointment_date_bloc_test.mocks.dart';

@GenerateMocks([GetAppointmentDateUseCase])
void main() {
  late MockGetAppointmentDateUseCase getAppointmentDateUseCaseMock;
  final dummyResponse = DateAppointmentModel();
  setUp(() {
    getAppointmentDateUseCaseMock = MockGetAppointmentDateUseCase();

    ///
    when(getAppointmentDateUseCaseMock.call()).thenAnswer((_) async => Right(dummyResponse));
  });

  ///
  blocTest<GetAppointmentDateCubit, GetAppointmentState>('Track get appointment date method',
      build: () =>
          GetAppointmentDateCubit(getAppointmentDateUseCase: getAppointmentDateUseCaseMock),
      act: (cubit) => cubit.getAppointmentDate(),
      expect: () => [isA<GetAppointmentLoadingState>(), isA<GetAppointmentSuccessState>()],
      verify: (cubit) {
        verify(getAppointmentDateUseCaseMock.call());
      });
}
