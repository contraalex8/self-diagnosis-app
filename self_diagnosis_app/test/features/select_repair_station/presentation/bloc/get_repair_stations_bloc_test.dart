import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/models/repair_station_model.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/usecases/repair_stations_usecase.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/usecases/sort_repair_stations_usecase.dart';
import 'package:self_diagnosis_app/features/select_repair_station/presentation/bloc/get_repair_stations_bloc/get_repair_stations_bloc.dart';

import 'get_repair_stations_bloc_test.mocks.dart';

@GenerateMocks([
  GetRepairStationsUseCase,
  SortRepairStationsUseCase,
])
void main() {
  late MockGetRepairStationsUseCase getRepairStationsUseCaseMock;
  late MockSortRepairStationsUseCase sortRepairStationsUseCaseMock;
  final dummyResponse = [
    RepairStationModel(name: 'Test1', price: 1341, time: 12, rating: 4.3),
    RepairStationModel(name: 'Test2', price: 322131, time: 2, rating: 5)
  ];
  setUp(() {
    getRepairStationsUseCaseMock = MockGetRepairStationsUseCase();
    sortRepairStationsUseCaseMock = MockSortRepairStationsUseCase();

    ///
    when(getRepairStationsUseCaseMock.call()).thenAnswer((_) async => Right(dummyResponse));

    ///
    when(sortRepairStationsUseCaseMock.callSortByPrice(dummyResponse))
        .thenAnswer((_) async => Right(dummyResponse));

    ///
    when(sortRepairStationsUseCaseMock.callSortByTime(dummyResponse))
        .thenAnswer((_) async => Right(dummyResponse));

    ///
    when(sortRepairStationsUseCaseMock.callSortByRating(dummyResponse))
        .thenAnswer((_) async => Right(dummyResponse));
  });

  group('GetRepairStationCubit tests', () {
    blocTest<GetRepairStationsCubit, GetRepairStationState>('Track get repair stations method',
        build: () => GetRepairStationsCubit(
              getRepairStationsUseCase: getRepairStationsUseCaseMock,
              sortRepairStationsUseCase: sortRepairStationsUseCaseMock,
            ),
        act: (cubit) => cubit.getRepairStations(),
        expect: () => [isA<GetRepairStationLoadingState>(), isA<GetRepairStationSuccessState>()],
        verify: (cubit) {
          verify(getRepairStationsUseCaseMock.call());
        });

    ///
    blocTest<GetRepairStationsCubit, GetRepairStationState>('Track sort method case sort-price',
        build: () => GetRepairStationsCubit(
              getRepairStationsUseCase: getRepairStationsUseCaseMock,
              sortRepairStationsUseCase: sortRepairStationsUseCaseMock,
            ),
        act: (cubit) => cubit.sortRepairStation('sort-price', dummyResponse),
        expect: () => [isA<GetRepairStationLoadingState>(), isA<GetRepairStationSuccessState>()],
        verify: (cubit) {
          verify(sortRepairStationsUseCaseMock.callSortByPrice(dummyResponse));
        });

    ///
    blocTest<GetRepairStationsCubit, GetRepairStationState>('Track sort method case sort-rating',
        build: () => GetRepairStationsCubit(
              getRepairStationsUseCase: getRepairStationsUseCaseMock,
              sortRepairStationsUseCase: sortRepairStationsUseCaseMock,
            ),
        act: (cubit) => cubit.sortRepairStation('sort-rating', dummyResponse),
        expect: () => [isA<GetRepairStationLoadingState>(), isA<GetRepairStationSuccessState>()],
        verify: (cubit) {
          verify(sortRepairStationsUseCaseMock.callSortByRating(dummyResponse));
        });

    ///
    blocTest<GetRepairStationsCubit, GetRepairStationState>('Track sort method case sort-time',
        build: () => GetRepairStationsCubit(
              getRepairStationsUseCase: getRepairStationsUseCaseMock,
              sortRepairStationsUseCase: sortRepairStationsUseCaseMock,
            ),
        act: (cubit) => cubit.sortRepairStation('sort-time', dummyResponse),
        expect: () => [isA<GetRepairStationLoadingState>(), isA<GetRepairStationSuccessState>()],
        verify: (cubit) {
          verify(sortRepairStationsUseCaseMock.callSortByTime(dummyResponse));
        });
  });
}
