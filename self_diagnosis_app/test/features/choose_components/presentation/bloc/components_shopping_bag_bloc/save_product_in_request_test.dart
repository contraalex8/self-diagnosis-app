import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/component_in_bag.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/usecases/components_shopping_bag_usecase.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/usecases/save_product_in_request_usecase.dart';
import 'package:self_diagnosis_app/features/choose_components/presentation/bloc/components_shopping_bag_bloc/components_shopping_bag_bloc.dart';

import 'save_product_in_request_test.mocks.dart';

@GenerateMocks([
  SaveProductInRequestUseCase,
  ComponentsShoppingBagUseCase,
])
void main() {
  // ignore: unused_local_variable
  late MockSaveProductInRequestUseCase saveProductInRequestUseCaseMock;
  late MockComponentsShoppingBagUseCase componentsShoppingBagUseCaseMock;

  final dummyModel = ProductModel(title: 'DummyTitle', price: 334);
  final dummyComponentModel = [ComponentInBagModel(), ComponentInBagModel()];
  final singleDummyComponentModel = ComponentInBagModel();

  setUp(() {
    saveProductInRequestUseCaseMock = MockSaveProductInRequestUseCase();
    componentsShoppingBagUseCaseMock = MockComponentsShoppingBagUseCase();

    ///
    when(saveProductInRequestUseCaseMock.call(dummyModel))
        .thenAnswer((_) async => const Right(true));

    ///
    when(componentsShoppingBagUseCaseMock.callGetContentBag())
        .thenAnswer((_) async => Right(dummyComponentModel));

    ///
    when(componentsShoppingBagUseCaseMock.callRemoveContentFromBag(
            dummyComponentModel, singleDummyComponentModel))
        .thenAnswer((_) async => Right(dummyComponentModel));
  });
  group('Components Shopping Bag  cubit tests', () {
    blocTest<ComponentsShoppingBagCubit, ComponentsShoppingBagState>(
        'Track get list of save product method',
        build: () => ComponentsShoppingBagCubit(
            saveProductInRequestUseCase: saveProductInRequestUseCaseMock,
            componentsShoppingBagUseCase: componentsShoppingBagUseCaseMock),
        act: (cubit) => cubit.saveProduct(dummyModel),
        expect: () => [
              isA<ComponentsShoppingBagInitialState>(),
              isA<ComponentsShoppingBagLoadingState>(),
              isA<ComponentsShoppingBagSaveProductSuccessState>()
            ],
        verify: (cubit) {
          verify(saveProductInRequestUseCaseMock.call(dummyModel));
        });

    blocTest<ComponentsShoppingBagCubit, ComponentsShoppingBagState>(
        'Track get component model list',
        build: () => ComponentsShoppingBagCubit(
            saveProductInRequestUseCase: saveProductInRequestUseCaseMock,
            componentsShoppingBagUseCase: componentsShoppingBagUseCaseMock),
        act: (cubit) => cubit.getProductsShoppingCart(),
        expect: () =>
            [isA<ComponentsShoppingBagLoadingState>(), isA<ComponentsShoppingBagSuccessState>()],
        verify: (cubit) {
          verify(componentsShoppingBagUseCaseMock.callGetContentBag());
        });

    blocTest<ComponentsShoppingBagCubit, ComponentsShoppingBagState>(
        'Track  remove component from components bag',
        build: () => ComponentsShoppingBagCubit(
            saveProductInRequestUseCase: saveProductInRequestUseCaseMock,
            componentsShoppingBagUseCase: componentsShoppingBagUseCaseMock),
        act: (cubit) => cubit.removeDataFromCart(dummyComponentModel, singleDummyComponentModel),
        expect: () => [
              isA<ComponentsShoppingBagInitialState>(),
              isA<ComponentsShoppingBagLoadingState>(),
              isA<ComponentsShoppingBagSuccessState>()
            ],
        verify: (cubit) {
          verify(componentsShoppingBagUseCaseMock.callRemoveContentFromBag(
              dummyComponentModel, singleDummyComponentModel));
        });
  });
}
