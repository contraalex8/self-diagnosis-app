import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/usecases/get_list_of_products_usecase.dart';
import 'package:self_diagnosis_app/features/choose_components/presentation/bloc/get_all_products_bloc/get_all_products_bloc.dart';

import 'get_all_products_bloc_test.mocks.dart';

@GenerateMocks([GetListOfProductsUseCase])
void main() {
  late MockGetListOfProductsUseCase getListOfProductsUseCaseMock;
  final dummyResponse = [
    ProductModel(title: 'DummyTitle', price: 334),
    ProductModel(title: 'DummyTitle2', price: 664),
    ProductModel(title: 'DummyTitle3', price: 900)
  ];
  setUp(() {
    getListOfProductsUseCaseMock = MockGetListOfProductsUseCase();
    when(getListOfProductsUseCaseMock.call()).thenAnswer((_) async => Right(dummyResponse));
  });
  group('Get all products cubit tests', () {
    blocTest<GetProductsCubit, GetProductsState>('Track get list of product method',
        build: () => GetProductsCubit(getListOfProductsUseCase: getListOfProductsUseCaseMock),
        act: (cubit) => cubit.getAllProducts(),
        expect: () => [isA<GetProductsStateLoadingState>(), isA<GetProductsStateSuccessState>()],
        verify: (cubit) {
          verify(getListOfProductsUseCaseMock.call());
        });
  });
}
