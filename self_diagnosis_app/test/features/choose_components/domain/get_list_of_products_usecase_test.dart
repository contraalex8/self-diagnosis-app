import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/repositories/get_list_of_products_repository.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/usecases/get_list_of_products_usecase.dart';

import 'get_list_of_products_usecase_test.mocks.dart';

@GenerateMocks([GetListOfProductsRepository])
void main() {
  late MockGetListOfProductsRepository getListOfProductsRepositoryMock;
  late GetListOfProductsUseCase previewAllProductsUseCase;
  final dummyResponse = [
    ProductModel(title: 'DummyTitle', price: 334),
    ProductModel(title: 'DummyTitle2', price: 664),
    ProductModel(title: 'DummyTitle3', price: 900)
  ];
  setUp(() {
    getListOfProductsRepositoryMock = MockGetListOfProductsRepository();
    previewAllProductsUseCase = GetListOfProductsUseCase(getListOfProductsRepositoryMock);
  });

  test('Get list of products data usecase test', () async {
    //set-up
    when(getListOfProductsRepositoryMock.getProductList())
        .thenAnswer((_) async => Right(dummyResponse));
    //act
    await previewAllProductsUseCase.call();
    //verify
    verify(getListOfProductsRepositoryMock.getProductList());
  });
}
