import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/repositories/save_product_in_request_repository.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/usecases/save_product_in_request_usecase.dart';

import 'save_products_in_request_usecase_test.mocks.dart';

@GenerateMocks([SaveProductRepository])
void main() {
  late MockSaveProductRepository saveProductRepositoryMock;
  late SaveProductInRequestUseCase saveProductInRequestUseCase;
  final dummyModel = ProductModel(title: 'DummyTitle', price: 334);
  setUp(() {
    saveProductRepositoryMock = MockSaveProductRepository();
    saveProductInRequestUseCase = SaveProductInRequestUseCase(saveProductRepositoryMock);
  });

  test('Preview data usecase test', () async {
    //set-up
    when(saveProductRepositoryMock.saveProduct(dummyModel)).thenAnswer((_) async => Right(true));
    //act
    await saveProductInRequestUseCase.call(dummyModel);
    //verify
    verify(saveProductRepositoryMock.saveProduct(dummyModel));
  });
}
