import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';
import 'package:self_diagnosis_app/features/select_date/domain/usecases/save_date_of_appointment_usecase.dart';
import 'package:self_diagnosis_app/features/select_date/presentation/bloc/save_date_of_appointment_bloc.dart';

import 'save_date_of_appointment_bloc_test.mocks.dart';

@GenerateMocks([SaveDateAppointmentUseCase])
void main() {
  late MockSaveDateAppointmentUseCase saveDateAppointmentUseCase;
  final dummyModel = DateAppointmentModel();
  setUp(() {
    saveDateAppointmentUseCase = MockSaveDateAppointmentUseCase();

    ///
    when(saveDateAppointmentUseCase.call(dummyModel)).thenAnswer((_) async => const Right(true));
  });
  group('Save data appointment cubit tests', () {
    blocTest<SaveDateAppointmentCubit, SaveDateOfAppointmentState>('Track save data method',
        build: () =>
            SaveDateAppointmentCubit(saveDateAppointmentUseCase: saveDateAppointmentUseCase),
        act: (cubit) => cubit.saveDateAppointment(dummyModel),
        expect: () =>
            [isA<SaveDateOfAppointmentLoadingState>(), isA<SaveDateOfAppointmentSuccessState>()],
        verify: (cubit) {
          verify(saveDateAppointmentUseCase.call(dummyModel));
        });
  });
}
