import 'package:flutter/material.dart';

import '../../core/dependency_injection/injection_container.dart' as di;
import '../application/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.initContainer();
  runApp(const MyApp());
}
