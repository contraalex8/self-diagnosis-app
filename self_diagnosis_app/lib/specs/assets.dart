// ignore_for_file: non_constant_identifier_names
class Directories {
  ///Gets data from [assets/get_components]
  static String getComponentsDirectory = 'assets/get_components';
}

class Assets {
  static String addIcon = '${Directories.getComponentsDirectory}/add.svg';
  static String removeIcon = '${Directories.getComponentsDirectory}/remove.svg';
}
