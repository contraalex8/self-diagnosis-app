// ignore_for_file: non_constant_identifier_names

class AppRadius {
  static double RADIUS_25 = 25.0;
  static double RADIUS_20 = 20.0;
  static double RADIUS_10 = 10.0;
  static double RADIUS_3 = 3.0;
}
