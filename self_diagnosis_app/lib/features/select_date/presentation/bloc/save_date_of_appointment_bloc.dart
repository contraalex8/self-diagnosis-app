import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';

import '../../../../../core/error/failures.dart';
import '../../domain/usecases/save_date_of_appointment_usecase.dart';

part 'save_date_of_appointment_state.dart';

class SaveDateAppointmentCubit extends Cubit<SaveDateOfAppointmentState> {
  final SaveDateAppointmentUseCase saveDateAppointmentUseCase;
  SaveDateAppointmentCubit({required this.saveDateAppointmentUseCase})
      : super(SaveDateOfAppointmentInitialState());

  void saveDateAppointment(DateAppointmentModel appointmentModel) async {
    emit(SaveDateOfAppointmentLoadingState());
    final failureOrEntity = await saveDateAppointmentUseCase(appointmentModel);
    if (failureOrEntity.isRight()) {
      emit(failureOrEntity.foldRight(
          SaveDateOfAppointmentFailureState(failure: const StandardFailure('Not saved')),
          (r, previous) => SaveDateOfAppointmentSuccessState()));
    }
  }
}
