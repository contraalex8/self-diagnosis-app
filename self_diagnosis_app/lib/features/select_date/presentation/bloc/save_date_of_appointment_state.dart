part of 'save_date_of_appointment_bloc.dart';

abstract class SaveDateOfAppointmentState extends Equatable {
  @override
  List<Object?> get props => [];
}

class SaveDateOfAppointmentInitialState extends SaveDateOfAppointmentState {}

class SaveDateOfAppointmentLoadingState extends SaveDateOfAppointmentState {}

class SaveDateOfAppointmentSuccessState extends SaveDateOfAppointmentState {}

class SaveDateOfAppointmentFailureState extends SaveDateOfAppointmentState {
  final Failure failure;

  SaveDateOfAppointmentFailureState({required this.failure});

  @override
  List<Object?> get props => [failure];
}
