import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:self_diagnosis_app/core/common_widgets/buttons/standard_button.dart';
import 'package:self_diagnosis_app/core/dependency_injection/injection_module.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';
import 'package:self_diagnosis_app/specs/constraints.dart';

import '../bloc/save_date_of_appointment_bloc.dart';

class DatePickerProvider extends StatelessWidget {
  const DatePickerProvider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => InjectionModule.injector<SaveDateAppointmentCubit>(),
      child: const DatePicker(),
    );
  }
}

class DatePicker extends StatefulWidget {
  const DatePicker({Key? key}) : super(key: key);

  @override
  State<DatePicker> createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  final TextEditingController _controllerDate =
      TextEditingController(text: DateTime.now().toString());
  final TextEditingController _controllerTime = TextEditingController(
      text:
          '${DateTime.now().hour.toString()}:${DateTime.now().minute < 10 ? 0 : ''}${DateTime.now().minute.toString()}');

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SaveDateAppointmentCubit, SaveDateOfAppointmentState>(
        builder: (context, state) {
      return Padding(
        padding: EdgeInsets.only(left: Paddings.P_22, top: Paddings.P_22, right: Paddings.P_22),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            DateTimePicker(
              type: DateTimePickerType.date,
              dateMask: 'd MMMM yyyy',
              dateLabelText: "Data",
              firstDate: DateTime(2023),
              lastDate: DateTime(2030),
              icon: const Icon(Icons.calendar_month),
              timeLabelText: "Time",
              controller: _controllerDate,
              locale: const Locale('ro', 'RO'),
            ),
            DateTimePicker(
              type: DateTimePickerType.time,
              //timePickerEntryModeInput: true,
              controller: _controllerTime,
              icon: const Icon(Icons.access_time),
              timeLabelText: "Ora",
              firstDate: DateTime.now(),
              use24HourFormat: true,
              locale: const Locale('ro', 'RO'),
            ),
            Padding(
              padding: EdgeInsets.only(top: Paddings.P_32),
              child: Center(child: StandardButton(onPressed: () {
                if (_controllerDate.text.isNotEmpty && _controllerTime.text.isNotEmpty) {
                  DateAppointmentModel dateAppointment =
                      DateAppointmentModel(date: _controllerDate.text, hour: _controllerTime.text);
                  context.read<SaveDateAppointmentCubit>().saveDateAppointment(dateAppointment);
                  _controllerTime.clear();
                  _controllerDate.clear();
                }
              })),
            ),
          ],
        ),
      );
    });
  }
}
