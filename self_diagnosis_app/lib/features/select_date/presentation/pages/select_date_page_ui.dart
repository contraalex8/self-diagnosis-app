import 'package:flutter/material.dart';

import '../../../../core/common_widgets/app_bar/standard_app_bar.dart';
import '../../../../core/common_widgets/bottom_navigation/standard_bottom_navigation.dart';
import '../../../../core/routes/routes.dart';
import '../../../../specs/colors.dart';
import '../../../choose_components/presentation/widgets/page_header.dart';
import '../widgets/date_picker.dart';

class SelectDatePageView extends StatelessWidget {
  const SelectDatePageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.WHITE,
      appBar: StandardAppBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: const [
          PageHeader(step1: false),
          DatePickerProvider(),
        ],
      ),
      bottomSheet: StandardBottomNavigation(
        isSecondRoute: true,
        onPressedBack: () {
          Navigators.getToStage1Route(context);
        },
        onPressedNext: () {
          Navigators.getToStage3Route(context);
        },
      ),
    );
  }
}
