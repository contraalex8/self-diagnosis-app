import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';

abstract class SaveDateAppointmentDataSource {
  Future<bool> saveDateAppointment(DateAppointmentModel dateAppointmentModel);
}
