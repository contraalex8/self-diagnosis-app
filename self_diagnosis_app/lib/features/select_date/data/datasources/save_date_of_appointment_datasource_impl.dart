import 'dart:convert';

import 'package:self_diagnosis_app/features/select_date/data/datasources/save_date_of_appointment_datasource.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';

import '../../../../core/dependency_injection/injection_module.dart';
import '../../../../core/shared_preferences/shared_preferences.dart';
import '../../../../core/shared_preferences/shared_preferences_keys.dart';

///Save the new list in [SharedPreferencesKeys.shipComponents]

class SaveDateAppointmentDataSourceImpl implements SaveDateAppointmentDataSource {
  @override
  Future<bool> saveDateAppointment(DateAppointmentModel dateAppointmentModel) async {
    Map<String, dynamic> newAppointmentModel = dateAppointmentModel.toJson();
    String encodedAppointment = json.encode(newAppointmentModel);
    InjectionModule.injector<SharedPreferencesAbstract>()
        .setString(SharedPreferencesKeys.appointmentDate, encodedAppointment);

    return true;
  }
}
