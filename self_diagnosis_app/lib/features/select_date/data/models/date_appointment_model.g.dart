// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'date_appointment_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_DateAppointmentModel _$$_DateAppointmentModelFromJson(
        Map<String, dynamic> json) =>
    _$_DateAppointmentModel(
      date: json['date'] as String?,
      hour: json['hour'] as String?,
    );

Map<String, dynamic> _$$_DateAppointmentModelToJson(
        _$_DateAppointmentModel instance) =>
    <String, dynamic>{
      'date': instance.date,
      'hour': instance.hour,
    };
