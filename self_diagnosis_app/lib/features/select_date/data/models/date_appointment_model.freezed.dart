// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'date_appointment_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

DateAppointmentModel _$DateAppointmentModelFromJson(Map<String, dynamic> json) {
  return _DateAppointmentModel.fromJson(json);
}

/// @nodoc
mixin _$DateAppointmentModel {
  String? get date => throw _privateConstructorUsedError;
  String? get hour => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DateAppointmentModelCopyWith<DateAppointmentModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DateAppointmentModelCopyWith<$Res> {
  factory $DateAppointmentModelCopyWith(DateAppointmentModel value,
          $Res Function(DateAppointmentModel) then) =
      _$DateAppointmentModelCopyWithImpl<$Res, DateAppointmentModel>;
  @useResult
  $Res call({String? date, String? hour});
}

/// @nodoc
class _$DateAppointmentModelCopyWithImpl<$Res,
        $Val extends DateAppointmentModel>
    implements $DateAppointmentModelCopyWith<$Res> {
  _$DateAppointmentModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? date = freezed,
    Object? hour = freezed,
  }) {
    return _then(_value.copyWith(
      date: freezed == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String?,
      hour: freezed == hour
          ? _value.hour
          : hour // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DateAppointmentModelCopyWith<$Res>
    implements $DateAppointmentModelCopyWith<$Res> {
  factory _$$_DateAppointmentModelCopyWith(_$_DateAppointmentModel value,
          $Res Function(_$_DateAppointmentModel) then) =
      __$$_DateAppointmentModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? date, String? hour});
}

/// @nodoc
class __$$_DateAppointmentModelCopyWithImpl<$Res>
    extends _$DateAppointmentModelCopyWithImpl<$Res, _$_DateAppointmentModel>
    implements _$$_DateAppointmentModelCopyWith<$Res> {
  __$$_DateAppointmentModelCopyWithImpl(_$_DateAppointmentModel _value,
      $Res Function(_$_DateAppointmentModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? date = freezed,
    Object? hour = freezed,
  }) {
    return _then(_$_DateAppointmentModel(
      date: freezed == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String?,
      hour: freezed == hour
          ? _value.hour
          : hour // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DateAppointmentModel implements _DateAppointmentModel {
  _$_DateAppointmentModel({this.date, this.hour});

  factory _$_DateAppointmentModel.fromJson(Map<String, dynamic> json) =>
      _$$_DateAppointmentModelFromJson(json);

  @override
  final String? date;
  @override
  final String? hour;

  @override
  String toString() {
    return 'DateAppointmentModel(date: $date, hour: $hour)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DateAppointmentModel &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.hour, hour) || other.hour == hour));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, date, hour);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DateAppointmentModelCopyWith<_$_DateAppointmentModel> get copyWith =>
      __$$_DateAppointmentModelCopyWithImpl<_$_DateAppointmentModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DateAppointmentModelToJson(
      this,
    );
  }
}

abstract class _DateAppointmentModel implements DateAppointmentModel {
  factory _DateAppointmentModel({final String? date, final String? hour}) =
      _$_DateAppointmentModel;

  factory _DateAppointmentModel.fromJson(Map<String, dynamic> json) =
      _$_DateAppointmentModel.fromJson;

  @override
  String? get date;
  @override
  String? get hour;
  @override
  @JsonKey(ignore: true)
  _$$_DateAppointmentModelCopyWith<_$_DateAppointmentModel> get copyWith =>
      throw _privateConstructorUsedError;
}
