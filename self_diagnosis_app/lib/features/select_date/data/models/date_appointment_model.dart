import 'package:freezed_annotation/freezed_annotation.dart';

part 'date_appointment_model.freezed.dart';
part 'date_appointment_model.g.dart';

@freezed
class DateAppointmentModel with _$DateAppointmentModel {
  factory DateAppointmentModel({
    String? date,
    String? hour,
  }) = _DateAppointmentModel;

  factory DateAppointmentModel.fromJson(Map<String, dynamic> json) =>
      _$DateAppointmentModelFromJson(json);
}
