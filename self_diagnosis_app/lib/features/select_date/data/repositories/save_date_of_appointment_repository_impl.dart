import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/core/error/failures.dart';
import 'package:self_diagnosis_app/features/select_date/data/datasources/save_date_of_appointment_datasource.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';

import '../../domain/repositories/save_date_of_appointment_repository.dart';

class SaveDateAppointmentRepositoryImpl implements SaveDateAppointmentRepository {
  final SaveDateAppointmentDataSource _saveDateAppointmentDataSource;
  SaveDateAppointmentRepositoryImpl({
    required SaveDateAppointmentDataSource saveDateAppointmentDataSource,
  }) : _saveDateAppointmentDataSource = saveDateAppointmentDataSource;

  @override
  Future<Either<Failure, bool>> saveDateAppointment(
      DateAppointmentModel dateAppointmentModel) async {
    try {
      final saveData =
          await _saveDateAppointmentDataSource.saveDateAppointment(dateAppointmentModel);
      return Right(saveData);
    } catch (e) {
      return const Left(StandardFailure('Standard failure error'));
    }
  }
}
