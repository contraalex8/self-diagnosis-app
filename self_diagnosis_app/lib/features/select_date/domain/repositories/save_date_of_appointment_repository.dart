import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/core/error/failures.dart';

import '../../data/models/date_appointment_model.dart';

abstract class SaveDateAppointmentRepository {
  Future<Either<Failure, bool>> saveDateAppointment(DateAppointmentModel dateAppointmentModel);
}
