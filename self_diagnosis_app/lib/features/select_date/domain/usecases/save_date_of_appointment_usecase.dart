import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';
import 'package:self_diagnosis_app/features/select_date/domain/repositories/save_date_of_appointment_repository.dart';

import '../../../../core/error/failures.dart';

class SaveDateAppointmentUseCase {
  final SaveDateAppointmentRepository saveDateAppointmentRepository;
  SaveDateAppointmentUseCase(this.saveDateAppointmentRepository);

  Future<Either<Failure, bool>> call(DateAppointmentModel dateAppointmentModel) async {
    return await saveDateAppointmentRepository.saveDateAppointment(dateAppointmentModel);
  }
}
