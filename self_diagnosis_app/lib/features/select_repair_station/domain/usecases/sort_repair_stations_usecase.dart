import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/models/repair_station_model.dart';

import '../../../../core/error/failures.dart';
import '../repositories/sort_repair_stations_repository.dart';

class SortRepairStationsUseCase {
  final SortRepairStationsRepository sortRepairStationsRepository;
  SortRepairStationsUseCase(this.sortRepairStationsRepository);

  Future<Either<Failure, List<RepairStationModel>>> callSortByPrice(
      List<RepairStationModel> stations) async {
    return await sortRepairStationsRepository.sortRepairStationsByPrice(stations);
  }

  Future<Either<Failure, List<RepairStationModel>>> callSortByRating(
      List<RepairStationModel> stations) async {
    return await sortRepairStationsRepository.sortRepairStationsByRating(stations);
  }

  Future<Either<Failure, List<RepairStationModel>>> callSortByTime(
      List<RepairStationModel> stations) async {
    return await sortRepairStationsRepository.sortRepairStationsByTime(stations);
  }
}
