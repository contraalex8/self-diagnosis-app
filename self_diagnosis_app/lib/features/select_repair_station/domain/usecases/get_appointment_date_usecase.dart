import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/repositories/get_appointment_date_repository.dart';

import '../../../../core/error/failures.dart';

class GetAppointmentDateUseCase {
  final GetAppointmentDateRepository getAppointmentDateDataRepository;
  GetAppointmentDateUseCase(this.getAppointmentDateDataRepository);

  Future<Either<Failure, DateAppointmentModel>> call() async {
    return await getAppointmentDateDataRepository.getAppointmentDate();
  }
}
