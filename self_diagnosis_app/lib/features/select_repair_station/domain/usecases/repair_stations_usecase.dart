import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/models/repair_station_model.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/repositories/repair_stations_repository.dart';

import '../../../../core/error/failures.dart';

class GetRepairStationsUseCase {
  final GetRepairStationsRepository getRepairStationsRepository;
  GetRepairStationsUseCase(this.getRepairStationsRepository);

  Future<Either<Failure, List<RepairStationModel>>> call() async {
    return await getRepairStationsRepository.getRepairStations();
  }
}
