import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/core/error/failures.dart';

import '../../data/models/repair_station_model.dart';

abstract class SortRepairStationsRepository {
  Future<Either<Failure, List<RepairStationModel>>> sortRepairStationsByPrice(
      List<RepairStationModel> stations);

  Future<Either<Failure, List<RepairStationModel>>> sortRepairStationsByRating(
      List<RepairStationModel> stations);

  Future<Either<Failure, List<RepairStationModel>>> sortRepairStationsByTime(
      List<RepairStationModel> stations);
}
