import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/core/error/failures.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';

abstract class GetAppointmentDateRepository {
  Future<Either<Failure, DateAppointmentModel>> getAppointmentDate();
}
