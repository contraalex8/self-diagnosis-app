import 'dart:convert';

import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';

import '../../../../core/dependency_injection/injection_module.dart';
import '../../../../core/shared_preferences/shared_preferences.dart';
import '../../../../core/shared_preferences/shared_preferences_keys.dart';
import 'get_appointment_date_datasource.dart';

class GetAppointmentDateDataSourceImpl implements GetAppointmentDateDataSource {
  @override
  Future<DateAppointmentModel> getAppointmentDate() async {
    String? currentAppointmentModel = InjectionModule.injector<SharedPreferencesAbstract>()
        .getString(SharedPreferencesKeys.appointmentDate);
    DateAppointmentModel? extractedModel =
        DateAppointmentModel.fromJson(json.decode(currentAppointmentModel!));
    return extractedModel;
  }
}
