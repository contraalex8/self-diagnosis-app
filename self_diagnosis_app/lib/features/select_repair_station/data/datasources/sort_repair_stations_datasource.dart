import 'package:self_diagnosis_app/features/select_repair_station/data/models/repair_station_model.dart';

abstract class SortRepairStationsDataSource {
  Future<List<RepairStationModel>> sortRepairStationByPrice(List<RepairStationModel> stations);
  Future<List<RepairStationModel>> sortRepairStationByRating(List<RepairStationModel> stations);
  Future<List<RepairStationModel>> sortRepairStationByTime(List<RepairStationModel> stations);
}
