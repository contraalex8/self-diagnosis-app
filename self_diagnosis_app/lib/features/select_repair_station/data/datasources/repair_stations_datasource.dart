import '../models/repair_station_model.dart';

abstract class RepairStationsDataSource {
  Future<List<RepairStationModel>> getRepairStationList();
}
