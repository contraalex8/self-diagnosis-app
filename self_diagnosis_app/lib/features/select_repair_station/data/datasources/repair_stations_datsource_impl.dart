import 'package:self_diagnosis_app/features/select_repair_station/data/datasources/repair_stations_datasource.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/models/repair_station_model.dart';

class RepairStationsDataSourceImpl implements RepairStationsDataSource {
  @override
  Future<List<RepairStationModel>> getRepairStationList() async {
    return [
      RepairStationModel(
        name: 'G\'s Garage',
        rating: 5.0,
        price: 1256.99,
        time: 2,
      ),
      RepairStationModel(
        name: 'Service PointS',
        rating: 3.4,
        price: 600.0,
        time: 4,
      ),
      RepairStationModel(
        name: 'Custom Service',
        rating: 5.0,
        price: 1000.0,
        time: 1.5,
      ),
      RepairStationModel(
        name: 'A\'s Garage',
        rating: 4.9,
        price: 1256.99,
        time: 2,
      ),
      RepairStationModel(
        name: 'Fancy Garage',
        rating: 4.8,
        price: 1566.99,
        time: 1,
      ),
      RepairStationModel(
        name: 'ABAC Garage',
        rating: 5.0,
        price: 900,
        time: 0.5,
      ),
    ];
  }
}
