import '../../../select_date/data/models/date_appointment_model.dart';

abstract class GetAppointmentDateDataSource {
  Future<DateAppointmentModel> getAppointmentDate();
}
