import 'package:self_diagnosis_app/features/select_repair_station/data/datasources/sort_repair_stations_datasource.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/models/repair_station_model.dart';

class SortRepairStationsDataSourceImpl implements SortRepairStationsDataSource {
  @override
  Future<List<RepairStationModel>> sortRepairStationByPrice(
      List<RepairStationModel> stations) async {
    stations.sort((a, b) {
      return a.price!.compareTo(b.price as num);
    });
    return stations;
  }

  @override
  Future<List<RepairStationModel>> sortRepairStationByRating(
      List<RepairStationModel> stations) async {
    stations.sort((a, b) {
      return a.rating!.compareTo(b.rating as num);
    });
    return stations;
  }

  @override
  Future<List<RepairStationModel>> sortRepairStationByTime(
      List<RepairStationModel> stations) async {
    stations.sort((a, b) {
      return a.time!.compareTo(b.time as num);
    });
    return stations;
  }
}
