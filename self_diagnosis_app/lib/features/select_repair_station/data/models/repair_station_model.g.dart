// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repair_station_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_RepairStationModel _$$_RepairStationModelFromJson(
        Map<String, dynamic> json) =>
    _$_RepairStationModel(
      name: json['name'] as String?,
      rating: (json['rating'] as num?)?.toDouble(),
      price: (json['price'] as num?)?.toDouble(),
      time: (json['time'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$$_RepairStationModelToJson(
        _$_RepairStationModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'rating': instance.rating,
      'price': instance.price,
      'time': instance.time,
    };
