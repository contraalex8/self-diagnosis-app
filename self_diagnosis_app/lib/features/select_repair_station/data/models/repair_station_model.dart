import 'package:freezed_annotation/freezed_annotation.dart';

part 'repair_station_model.freezed.dart';
part 'repair_station_model.g.dart';

@freezed
class RepairStationModel with _$RepairStationModel {
  factory RepairStationModel({
    String? name,
    double? rating,
    double? price,
    double? time,
  }) = _RepairStationModel;

  factory RepairStationModel.fromJson(Map<String, dynamic> json) =>
      _$RepairStationModelFromJson(json);
}
