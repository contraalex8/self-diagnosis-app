// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'repair_station_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RepairStationModel _$RepairStationModelFromJson(Map<String, dynamic> json) {
  return _RepairStationModel.fromJson(json);
}

/// @nodoc
mixin _$RepairStationModel {
  String? get name => throw _privateConstructorUsedError;
  double? get rating => throw _privateConstructorUsedError;
  double? get price => throw _privateConstructorUsedError;
  double? get time => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RepairStationModelCopyWith<RepairStationModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RepairStationModelCopyWith<$Res> {
  factory $RepairStationModelCopyWith(
          RepairStationModel value, $Res Function(RepairStationModel) then) =
      _$RepairStationModelCopyWithImpl<$Res, RepairStationModel>;
  @useResult
  $Res call({String? name, double? rating, double? price, double? time});
}

/// @nodoc
class _$RepairStationModelCopyWithImpl<$Res, $Val extends RepairStationModel>
    implements $RepairStationModelCopyWith<$Res> {
  _$RepairStationModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = freezed,
    Object? rating = freezed,
    Object? price = freezed,
    Object? time = freezed,
  }) {
    return _then(_value.copyWith(
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      rating: freezed == rating
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double?,
      time: freezed == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as double?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_RepairStationModelCopyWith<$Res>
    implements $RepairStationModelCopyWith<$Res> {
  factory _$$_RepairStationModelCopyWith(_$_RepairStationModel value,
          $Res Function(_$_RepairStationModel) then) =
      __$$_RepairStationModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? name, double? rating, double? price, double? time});
}

/// @nodoc
class __$$_RepairStationModelCopyWithImpl<$Res>
    extends _$RepairStationModelCopyWithImpl<$Res, _$_RepairStationModel>
    implements _$$_RepairStationModelCopyWith<$Res> {
  __$$_RepairStationModelCopyWithImpl(
      _$_RepairStationModel _value, $Res Function(_$_RepairStationModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = freezed,
    Object? rating = freezed,
    Object? price = freezed,
    Object? time = freezed,
  }) {
    return _then(_$_RepairStationModel(
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      rating: freezed == rating
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double?,
      time: freezed == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_RepairStationModel implements _RepairStationModel {
  _$_RepairStationModel({this.name, this.rating, this.price, this.time});

  factory _$_RepairStationModel.fromJson(Map<String, dynamic> json) =>
      _$$_RepairStationModelFromJson(json);

  @override
  final String? name;
  @override
  final double? rating;
  @override
  final double? price;
  @override
  final double? time;

  @override
  String toString() {
    return 'RepairStationModel(name: $name, rating: $rating, price: $price, time: $time)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RepairStationModel &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.rating, rating) || other.rating == rating) &&
            (identical(other.price, price) || other.price == price) &&
            (identical(other.time, time) || other.time == time));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, name, rating, price, time);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RepairStationModelCopyWith<_$_RepairStationModel> get copyWith =>
      __$$_RepairStationModelCopyWithImpl<_$_RepairStationModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_RepairStationModelToJson(
      this,
    );
  }
}

abstract class _RepairStationModel implements RepairStationModel {
  factory _RepairStationModel(
      {final String? name,
      final double? rating,
      final double? price,
      final double? time}) = _$_RepairStationModel;

  factory _RepairStationModel.fromJson(Map<String, dynamic> json) =
      _$_RepairStationModel.fromJson;

  @override
  String? get name;
  @override
  double? get rating;
  @override
  double? get price;
  @override
  double? get time;
  @override
  @JsonKey(ignore: true)
  _$$_RepairStationModelCopyWith<_$_RepairStationModel> get copyWith =>
      throw _privateConstructorUsedError;
}
