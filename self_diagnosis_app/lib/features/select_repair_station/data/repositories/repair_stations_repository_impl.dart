import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/core/error/failures.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/datasources/repair_stations_datasource.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/models/repair_station_model.dart';

import '../../domain/repositories/repair_stations_repository.dart';

class GetRepairStationsRepositoryImpl implements GetRepairStationsRepository {
  final RepairStationsDataSource _repairStationsDataSource;
  GetRepairStationsRepositoryImpl({
    required RepairStationsDataSource repairStationsDataSource,
  }) : _repairStationsDataSource = repairStationsDataSource;

  @override
  Future<Either<Failure, List<RepairStationModel>>> getRepairStations() async {
    try {
      final getRepairStations = await _repairStationsDataSource.getRepairStationList();
      return Right(getRepairStations);
    } catch (e) {
      return const Left(StandardFailure('Standard failure error'));
    }
  }
}
