import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/core/error/failures.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/datasources/get_appointment_date_datasource.dart';

import '../../domain/repositories/get_appointment_date_repository.dart';

class GetAppointmentDateRepositoryImpl implements GetAppointmentDateRepository {
  final GetAppointmentDateDataSource _getAppointmentDateDataSource;
  GetAppointmentDateRepositoryImpl({
    required GetAppointmentDateDataSource getAppointmentDateDataSource,
  }) : _getAppointmentDateDataSource = getAppointmentDateDataSource;
  @override
  Future<Either<Failure, DateAppointmentModel>> getAppointmentDate() async {
    try {
      final getDate = await _getAppointmentDateDataSource.getAppointmentDate();
      return Right(getDate);
    } catch (e) {
      return const Left(StandardFailure('Standard failure error'));
    }
  }
}
