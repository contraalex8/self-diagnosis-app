import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/core/error/failures.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/datasources/sort_repair_stations_datasource.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/models/repair_station_model.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/repositories/sort_repair_stations_repository.dart';

class SortRepairStationsRepositoryImpl implements SortRepairStationsRepository {
  final SortRepairStationsDataSource _sortRepairStationsDataSource;
  SortRepairStationsRepositoryImpl({
    required SortRepairStationsDataSource sortRepairStationsDataSource,
  }) : _sortRepairStationsDataSource = sortRepairStationsDataSource;

  @override
  Future<Either<Failure, List<RepairStationModel>>> sortRepairStationsByPrice(
      List<RepairStationModel> stations) async {
    try {
      final getRepairStations =
          await _sortRepairStationsDataSource.sortRepairStationByPrice(stations);
      return Right(getRepairStations);
    } catch (e) {
      return const Left(StandardFailure('Standard failure error'));
    }
  }

  @override
  Future<Either<Failure, List<RepairStationModel>>> sortRepairStationsByRating(
      List<RepairStationModel> stations) async {
    try {
      final getRepairStations =
          await _sortRepairStationsDataSource.sortRepairStationByRating(stations);
      return Right(getRepairStations);
    } catch (e) {
      return const Left(StandardFailure('Standard failure error'));
    }
  }

  @override
  Future<Either<Failure, List<RepairStationModel>>> sortRepairStationsByTime(
      List<RepairStationModel> stations) async {
    try {
      final getRepairStations =
          await _sortRepairStationsDataSource.sortRepairStationByTime(stations);
      return Right(getRepairStations);
    } catch (e) {
      return const Left(StandardFailure('Standard failure error'));
    }
  }
}
