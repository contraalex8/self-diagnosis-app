part of 'get_repair_stations_bloc.dart';

class GetRepairStationState extends Equatable {
  @override
  List<Object?> get props => [];
}

class GetRepairStationInitialState extends GetRepairStationState {}

class GetRepairStationLoadingState extends GetRepairStationState {}

class GetRepairStationSuccessState extends GetRepairStationState {
  final List<RepairStationModel> response;

  GetRepairStationSuccessState({required this.response});

  @override
  List<Object?> get props => [response];
}

class GetRepairStationFailureState extends GetRepairStationState {
  final Failure failure;

  GetRepairStationFailureState({required this.failure});

  @override
  List<Object?> get props => [failure];
}
