import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/usecases/repair_stations_usecase.dart';

import '../../../../../core/error/failures.dart';
import '../../../data/models/repair_station_model.dart';
import '../../../domain/usecases/sort_repair_stations_usecase.dart';

part 'get_repair_stations_state.dart';

class GetRepairStationsCubit extends Cubit<GetRepairStationState> {
  final GetRepairStationsUseCase getRepairStationsUseCase;
  final SortRepairStationsUseCase sortRepairStationsUseCase;
  GetRepairStationsCubit({
    required this.getRepairStationsUseCase,
    required this.sortRepairStationsUseCase,
  }) : super(GetRepairStationInitialState());

  ///[isRating],[isPrice],[isTime] will be the values for check boxes
  late bool isRating = false;
  late bool isPrice = false;
  late bool isTime = false;

  void getRepairStations() async {
    emit(GetRepairStationLoadingState());
    final failureOrEntity = await getRepairStationsUseCase();
    if (failureOrEntity.isRight()) {
      emit(failureOrEntity.foldRight(
          GetRepairStationFailureState(failure: const StandardFailure('Repair stations error')),
          (r, previous) => GetRepairStationSuccessState(response: r)));
    }
  }

  void sortRepairStation(String fieldSorted, List<RepairStationModel> stations) async {
    switch (fieldSorted) {
      case 'sort-price':
        {
          if (isPrice) {
            isPrice = !isPrice;
            emit(GetRepairStationInitialState());
          } else {
            isPrice = true;
            emit(GetRepairStationLoadingState());
            final failureOrEntity = await sortRepairStationsUseCase.callSortByPrice(stations);
            if (failureOrEntity.isRight()) {
              emit(failureOrEntity.foldRight(
                  GetRepairStationFailureState(
                      failure: const StandardFailure('Repair stations error')),
                  (r, previous) => GetRepairStationSuccessState(response: r)));
            }
          }

          break;
        }
      case 'sort-rating':
        {
          if (isRating) {
            isRating = !isRating;
            emit(GetRepairStationInitialState());
          } else {
            isRating = true;
            emit(GetRepairStationLoadingState());
            final failureOrEntity = await sortRepairStationsUseCase.callSortByRating(stations);
            if (failureOrEntity.isRight()) {
              emit(failureOrEntity.foldRight(
                  GetRepairStationFailureState(
                      failure: const StandardFailure('Repair stations error')),
                  (r, previous) => GetRepairStationSuccessState(response: r)));
            }
          }
          break;
        }
      case 'sort-time':
        {
          if (isTime) {
            isTime = !isTime;
            emit(GetRepairStationInitialState());
          } else {
            isTime = true;
            emit(GetRepairStationLoadingState());
            final failureOrEntity = await sortRepairStationsUseCase.callSortByTime(stations);
            if (failureOrEntity.isRight()) {
              emit(failureOrEntity.foldRight(
                  GetRepairStationFailureState(
                      failure: const StandardFailure('Repair stations error')),
                  (r, previous) => GetRepairStationSuccessState(response: r)));
            }
          }
          break;
        }
    }
  }
}
