import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:self_diagnosis_app/features/select_date/data/models/date_appointment_model.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/usecases/get_appointment_date_usecase.dart';

import '../../../../../core/error/failures.dart';

part 'get_appointment_date_state.dart';

class GetAppointmentDateCubit extends Cubit<GetAppointmentState> {
  final GetAppointmentDateUseCase getAppointmentDateUseCase;
  GetAppointmentDateCubit({required this.getAppointmentDateUseCase})
      : super(GetAppointmentInitialState());

  void getAppointmentDate() async {
    emit(GetAppointmentLoadingState());
    final failureOrEntity = await getAppointmentDateUseCase();
    if (failureOrEntity.isRight()) {
      emit(failureOrEntity.foldRight(
          GetAppointmentFailureState(failure: const StandardFailure('Appointment not get')),
          (r, previous) => GetAppointmentSuccessState(response: r)));
    }
  }
}
