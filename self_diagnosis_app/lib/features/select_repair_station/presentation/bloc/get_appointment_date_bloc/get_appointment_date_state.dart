part of 'get_appointment_date_bloc.dart';

class GetAppointmentState extends Equatable {
  @override
  List<Object?> get props => [];
}

class GetAppointmentInitialState extends GetAppointmentState {}

class GetAppointmentLoadingState extends GetAppointmentState {}

class GetAppointmentSuccessState extends GetAppointmentState {
  final DateAppointmentModel response;

  GetAppointmentSuccessState({required this.response});

  @override
  List<Object?> get props => [response];
}

class GetAppointmentFailureState extends GetAppointmentState {
  final Failure failure;

  GetAppointmentFailureState({required this.failure});

  @override
  List<Object?> get props => [failure];
}
