import 'package:flutter/material.dart';
import 'package:self_diagnosis_app/features/select_repair_station/presentation/widgets/page_header_view_ui.dart';

import '../../../../core/common_widgets/app_bar/standard_app_bar.dart';
import '../../../../specs/colors.dart';
import '../widgets/search_repair_station_engine_view_ui.dart';

class SelectRepairStationView extends StatefulWidget {
  const SelectRepairStationView({Key? key}) : super(key: key);

  @override
  State<SelectRepairStationView> createState() => _SelectRepairStationViewState();
}

class _SelectRepairStationViewState extends State<SelectRepairStationView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.WHITE,
      appBar: StandardAppBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: const [
          PageHeaderViewProvider(),
          Expanded(child: SearchRepairStationProvider()),
        ],
      ),
    );
  }
}
