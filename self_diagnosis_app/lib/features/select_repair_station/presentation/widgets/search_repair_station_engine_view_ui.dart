import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:self_diagnosis_app/core/dependency_injection/injection_module.dart';
import 'package:self_diagnosis_app/features/select_repair_station/presentation/bloc/get_repair_stations_bloc/get_repair_stations_bloc.dart';
import 'package:self_diagnosis_app/features/select_repair_station/presentation/widgets/sorting_row.dart';

import '../../../../core/common_widgets/component_entry/component_characteristic_fields.dart';
import '../../../../core/common_widgets/text_fields/regular_text_field.dart';
import '../../../../specs/colors.dart';
import '../../../../specs/constraints.dart';
import '../../../../specs/radiuses.dart';
import '../../../../specs/text_sizes.dart';

class SearchRepairStationProvider extends StatelessWidget {
  const SearchRepairStationProvider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => InjectionModule.injector<GetRepairStationsCubit>(),
      child: const SearchRepairStationView(),
    );
  }
}

class SearchRepairStationView extends StatefulWidget {
  const SearchRepairStationView({Key? key}) : super(key: key);

  @override
  State<SearchRepairStationView> createState() => _SearchRepairStationViewState();
}

class _SearchRepairStationViewState extends State<SearchRepairStationView> {
  late bool _isRating = false;
  late bool _isPrice = false;
  late bool _isTime = false;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GetRepairStationsCubit, GetRepairStationState>(builder: (context, state) {
      if (state is GetRepairStationInitialState) {
        context.read<GetRepairStationsCubit>().getRepairStations();
      }
      if (state is GetRepairStationSuccessState) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding:
                  EdgeInsets.only(top: Paddings.P_22, left: Paddings.P_22, right: Paddings.P_22),
              child: SizedBox(
                child: RegularTextField(
                    validator: FormBuilderValidators.required(),
                    fieldName: 'search',
                    label: 'Cauta ofertant',
                    hint: 'Repair garage'),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: Paddings.P_22, top: Paddings.P_14),
              child: Text(
                "Sortare dupa: ",
                style: TextStyle(fontSize: TextSizes.SIZE_16, color: AppColors.DARK_GREEN),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: Paddings.P_22),
              child: Row(
                children: [
                  SortingRow(
                    onPressed: (value) {
                      context
                          .read<GetRepairStationsCubit>()
                          .sortRepairStation('sort-rating', state.response);
                    },
                    isPressed: context.read<GetRepairStationsCubit>().isRating,
                    text: 'Rating',
                  ),
                  SortingRow(
                    onPressed: (value) {
                      context
                          .read<GetRepairStationsCubit>()
                          .sortRepairStation('sort-price', state.response);
                    },
                    isPressed: context.read<GetRepairStationsCubit>().isPrice,
                    text: 'Pret',
                  ),
                  SortingRow(
                    onPressed: (value) {
                      context
                          .read<GetRepairStationsCubit>()
                          .sortRepairStation('sort-time', state.response);
                    },
                    isPressed: context.read<GetRepairStationsCubit>().isTime,
                    text: 'Timp',
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: state.response.length,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return Padding(
                        padding: EdgeInsets.only(bottom: Paddings.P_14, top: Paddings.P_14),
                        child: Container(
                          decoration: BoxDecoration(
                              color: AppColors.WHITE,
                              borderRadius: BorderRadius.circular(AppRadius.RADIUS_10)),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  ComponentCharacteristicField(
                                    label: 'Service',
                                    data: state.response[index].name,
                                  ),
                                  ComponentCharacteristicField(
                                    label: 'Rating',
                                    data: state.response[index].rating,
                                  ),
                                  ComponentCharacteristicField(
                                    label: 'Price',
                                    data: '${state.response[index].price} RON',
                                  ),
                                  ComponentCharacteristicField(
                                    label: 'Timp',
                                    data: '${state.response[index].time} zile',
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ));
                  }),
            ),
          ],
        );
      } else {
        return const Center(
          child: Text('Failed to fetch data'),
        );
      }
    });
  }
}
