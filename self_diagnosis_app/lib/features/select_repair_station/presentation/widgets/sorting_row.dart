import 'package:flutter/material.dart';

class SortingRow extends StatelessWidget {
  final void Function(bool?)? onPressed;
  final bool isPressed;
  final String text;
  const SortingRow({required this.onPressed, required this.isPressed, required this.text, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(value: isPressed, onChanged: onPressed),
        Text(text),
      ],
    );
  }
}
