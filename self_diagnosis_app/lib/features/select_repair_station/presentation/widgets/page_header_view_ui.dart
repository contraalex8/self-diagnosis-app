import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:self_diagnosis_app/core/dependency_injection/injection_module.dart';
import 'package:self_diagnosis_app/features/select_repair_station/presentation/bloc/get_appointment_date_bloc/get_appointment_date_bloc.dart';
import 'package:self_diagnosis_app/specs/colors.dart';
import 'package:self_diagnosis_app/specs/constraints.dart';
import 'package:self_diagnosis_app/specs/text_sizes.dart';

class PageHeaderViewProvider extends StatelessWidget {
  const PageHeaderViewProvider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => InjectionModule.injector<GetAppointmentDateCubit>(),
      child: const PageHeaderView(),
    );
  }
}

class PageHeaderView extends StatelessWidget {
  const PageHeaderView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GetAppointmentDateCubit, GetAppointmentState>(builder: (context, state) {
      if (state is GetAppointmentInitialState) {
        context.read<GetAppointmentDateCubit>().getAppointmentDate();
      }
      if (state is GetAppointmentSuccessState) {
        return Column(
          children: [
            Center(
              child: Text(
                'Programarea P1',
                style: TextStyle(fontSize: TextSizes.SIZE_20, color: AppColors.DARK_GREEN),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: Paddings.P_14),
              child: Center(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Text(
                        state.response.date ?? '',
                        style: TextStyle(fontSize: TextSizes.SIZE_20, color: AppColors.DARK_GREEN),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: Paddings.P_14),
                      child: Center(
                        child: Text(
                          state.response.hour ?? '',
                          style:
                              TextStyle(fontSize: TextSizes.SIZE_20, color: AppColors.DARK_GREEN),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        );
      } else {
        return const Center(
          child: Text('Failed to get data'),
        );
      }
    });
  }
}
