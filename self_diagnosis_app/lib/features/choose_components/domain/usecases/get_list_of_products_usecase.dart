import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';

import '../../../../core/error/failures.dart';
import '../repositories/get_list_of_products_repository.dart';

class GetListOfProductsUseCase {
  final GetListOfProductsRepository previewAllProductsRepository;
  GetListOfProductsUseCase(this.previewAllProductsRepository);

  Future<Either<Failure, List<ProductModel>>> call() async {
    return await previewAllProductsRepository.getProductList();
  }
}
