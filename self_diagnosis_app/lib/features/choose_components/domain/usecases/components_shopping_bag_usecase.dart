import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/component_in_bag.dart';

import '../../../../core/error/failures.dart';
import '../repositories/components_shopping_bag_repository.dart';

class ComponentsShoppingBagUseCase {
  final ComponentsShoppingBagRepository componentsShoppingBagRepository;
  ComponentsShoppingBagUseCase(this.componentsShoppingBagRepository);

  Future<Either<Failure, List<ComponentInBagModel>>> callGetContentBag() async {
    return await componentsShoppingBagRepository.getBagContent();
  }

  Future<Either<Failure, List<ComponentInBagModel>>> callRemoveContentFromBag(
      List<ComponentInBagModel> currentList, ComponentInBagModel product) async {
    return await componentsShoppingBagRepository.removeBagContent(currentList, product);
  }
}
