import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/repositories/save_product_in_request_repository.dart';

import '../../../../core/error/failures.dart';

class SaveProductInRequestUseCase {
  final SaveProductRepository saveProductRepository;
  SaveProductInRequestUseCase(this.saveProductRepository);

  Future<Either<Failure, bool>> call(ProductModel product) async {
    return await saveProductRepository.saveProduct(product);
  }
}
