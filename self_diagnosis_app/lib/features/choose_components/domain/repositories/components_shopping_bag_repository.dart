import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../data/models/component_in_bag.dart';

abstract class ComponentsShoppingBagRepository {
  Future<Either<Failure, List<ComponentInBagModel>>> removeBagContent(
      List<ComponentInBagModel> currentList, ComponentInBagModel product);
  Future<Either<Failure, List<ComponentInBagModel>>> getBagContent();
}
