import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';

import '../../../../core/error/failures.dart';

abstract class GetListOfProductsRepository {
  Future<Either<Failure, List<ProductModel>>> getProductList();
}
