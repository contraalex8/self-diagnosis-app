import 'package:flutter/material.dart';
import 'package:self_diagnosis_app/core/common_widgets/app_bar/standard_app_bar.dart';
import 'package:self_diagnosis_app/core/common_widgets/bottom_navigation/standard_bottom_navigation.dart';
import 'package:self_diagnosis_app/core/routes/routes.dart';
import 'package:self_diagnosis_app/features/choose_components/presentation/widgets/page_header.dart';
import 'package:self_diagnosis_app/features/choose_components/presentation/widgets/search_engine.dart';
import 'package:self_diagnosis_app/specs/colors.dart';

import '../../../../core/dependency_injection/injection_module.dart';
import '../../../../core/shared_preferences/shared_preferences.dart';
import '../../../../core/shared_preferences/shared_preferences_keys.dart';
import '../widgets/component_list.dart';

class ChooseComponentsView extends StatefulWidget {
  const ChooseComponentsView({Key? key}) : super(key: key);

  @override
  State<ChooseComponentsView> createState() => _ChooseComponentsViewState();
}

class _ChooseComponentsViewState extends State<ChooseComponentsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.WHITE,
      appBar: StandardAppBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: const [
          PageHeader(step1: true),
          SearchEngineProvider(),
          Expanded(child: ComponentsListProvider()),
        ],
      ),
      bottomSheet: StandardBottomNavigation(
        isSecondRoute: false,
        onPressedBack: () {},
        onPressedNext: () {
          if (InjectionModule.injector<SharedPreferencesAbstract>()
                  .getString(SharedPreferencesKeys.shipComponents) !=
              'shipComponents') {
            Navigators.getToStage2Route(context);
          }
        },
      ),
    );
  }
}
