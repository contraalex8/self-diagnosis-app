part of 'components_shopping_bag_bloc.dart';

abstract class ComponentsShoppingBagState extends Equatable {
  @override
  List<Object?> get props => [];
}

class ComponentsShoppingBagInitialState extends ComponentsShoppingBagState {}

class ComponentsShoppingBagLoadingState extends ComponentsShoppingBagState {}

class ComponentsShoppingBagSuccessState extends ComponentsShoppingBagState {
  final List<ComponentInBagModel> response;

  ComponentsShoppingBagSuccessState({required this.response});

  @override
  List<Object?> get props => [response];
}

class ComponentsShoppingBagSaveProductSuccessState extends ComponentsShoppingBagState {}

class ComponentsShoppingBagEmptyListState extends ComponentsShoppingBagState {
  final List<ComponentInBagModel> response;

  ComponentsShoppingBagEmptyListState({required this.response});

  @override
  List<Object?> get props => [response];
}

class ComponentsShoppingBagFailureState extends ComponentsShoppingBagState {
  final Failure failure;

  ComponentsShoppingBagFailureState({required this.failure});

  @override
  List<Object?> get props => [failure];
}
