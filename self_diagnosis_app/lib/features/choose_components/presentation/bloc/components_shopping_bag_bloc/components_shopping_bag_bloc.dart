import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:self_diagnosis_app/core/error/failures.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/component_in_bag.dart';

import '../../../data/models/product_model.dart';
import '../../../domain/usecases/components_shopping_bag_usecase.dart';
import '../../../domain/usecases/save_product_in_request_usecase.dart';

part 'components_shopping_bag_state.dart';

class ComponentsShoppingBagCubit extends Cubit<ComponentsShoppingBagState> {
  final ComponentsShoppingBagUseCase componentsShoppingBagUseCase;
  final SaveProductInRequestUseCase saveProductInRequestUseCase;
  ComponentsShoppingBagCubit({
    required this.componentsShoppingBagUseCase,
    required this.saveProductInRequestUseCase,
  }) : super(ComponentsShoppingBagInitialState());

  Future<void> saveProduct(ProductModel product) async {
    emit(ComponentsShoppingBagInitialState());
    emit(ComponentsShoppingBagLoadingState());

    final failureOrEntity = await saveProductInRequestUseCase(product);
    if (failureOrEntity.isRight()) {
      emit(failureOrEntity.foldRight(
          ComponentsShoppingBagFailureState(failure: const StandardFailure('Products not saved!')),
          (r, previous) => ComponentsShoppingBagSaveProductSuccessState()));
    }
  }

  void getProductsShoppingCart() async {
    emit(ComponentsShoppingBagLoadingState());
    final failureOrEntity = await componentsShoppingBagUseCase.callGetContentBag();
    if (failureOrEntity.isRight()) {
      emit(
        failureOrEntity.foldRight(
            ComponentsShoppingBagFailureState(failure: const StandardFailure('Failed to get data')),
            (r, previous) {
          if (r.isEmpty) {
            return ComponentsShoppingBagEmptyListState(response: r);
          } else {
            return ComponentsShoppingBagSuccessState(response: r);
          }
        }),
      );
    } else {
      emit(ComponentsShoppingBagFailureState(failure: const StandardFailure('Failed to get data')));
    }
  }

  void removeDataFromCart(
      List<ComponentInBagModel> currentList, ComponentInBagModel product) async {
    emit(ComponentsShoppingBagInitialState());
    emit(ComponentsShoppingBagLoadingState());
    final failureOrEntity =
        await componentsShoppingBagUseCase.callRemoveContentFromBag(currentList, product);
    if (failureOrEntity.isRight()) {
      emit(
        failureOrEntity.foldRight(
            ComponentsShoppingBagFailureState(
                failure: const StandardFailure('Failed to remove data')), (r, previous) {
          if (r.isEmpty) {
            return ComponentsShoppingBagEmptyListState(response: r);
          } else {
            return ComponentsShoppingBagSuccessState(response: r);
          }
        }),
      );
    } else {
      emit(ComponentsShoppingBagFailureState(
          failure: const StandardFailure('Failed to remove data')));
    }
  }
}
