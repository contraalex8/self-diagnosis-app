part of 'get_all_products_bloc.dart';

abstract class GetProductsState extends Equatable {
  @override
  List<Object?> get props => [];
}

class GetProductsStateInitialState extends GetProductsState {}

class GetProductsStateLoadingState extends GetProductsState {}

class GetProductsStateSuccessState extends GetProductsState {
  final List<ProductModel> response;

  GetProductsStateSuccessState({required this.response});

  @override
  List<Object?> get props => [response];
}

class GetProductsStateFailureState extends GetProductsState {
  final Failure failure;

  GetProductsStateFailureState({required this.failure});

  @override
  List<Object?> get props => [failure];
}
