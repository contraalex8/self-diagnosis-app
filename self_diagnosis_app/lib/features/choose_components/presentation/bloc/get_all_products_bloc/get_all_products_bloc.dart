import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/usecases/get_list_of_products_usecase.dart';

import '../../../../../core/error/failures.dart';
import '../../../data/models/product_model.dart';

part 'get_all_products_state.dart';

class GetProductsCubit extends Cubit<GetProductsState> {
  final GetListOfProductsUseCase getListOfProductsUseCase;
  GetProductsCubit({required this.getListOfProductsUseCase})
      : super(GetProductsStateInitialState());

  void getAllProducts() async {
    emit(GetProductsStateLoadingState());
    final failureOrEntity = await getListOfProductsUseCase();
    if (failureOrEntity.isRight()) {
      emit(failureOrEntity.foldRight(
          GetProductsStateFailureState(failure: const StandardFailure('Products not found!')),
          (r, previous) => GetProductsStateSuccessState(response: r)));
    }
  }

  String checkIfProductExists(List<ProductModel> products, String input) {
    String currentProduct = 'not-a-match';
    for (var product in products) {
      if (product.title == input) {
        currentProduct = product.title ?? 'nullable-string';
      }
    }
    return currentProduct;
  }

  ProductModel getProduct(List<ProductModel> products, String searchProduct) {
    ProductModel currentProduct = ProductModel();
    for (var product in products) {
      if (product.title == searchProduct) {
        currentProduct = product;
      }
    }
    return currentProduct;
  }
}
