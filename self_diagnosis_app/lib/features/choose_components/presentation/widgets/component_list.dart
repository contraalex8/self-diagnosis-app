import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:self_diagnosis_app/core/dependency_injection/injection_module.dart';
import 'package:self_diagnosis_app/features/choose_components/presentation/bloc/components_shopping_bag_bloc/components_shopping_bag_bloc.dart';
import 'package:self_diagnosis_app/specs/constraints.dart';

import '../../../../core/common_widgets/component_entry/component_characteristic_fields.dart';
import '../../../../specs/assets.dart';
import '../../../../specs/colors.dart';
import '../../../../specs/radiuses.dart';

class ComponentsListProvider extends StatelessWidget {
  const ComponentsListProvider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => InjectionModule.injector<ComponentsShoppingBagCubit>(),
      child: const ComponentList(),
    );
  }
}

class ComponentList extends StatelessWidget {
  const ComponentList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ComponentsShoppingBagCubit, ComponentsShoppingBagState>(
      builder: (BuildContext context, state) {
        if (state is ComponentsShoppingBagInitialState) {
          context.read<ComponentsShoppingBagCubit>().getProductsShoppingCart();
        }
        if (state is ComponentsShoppingBagSaveProductSuccessState) {
          context.read<ComponentsShoppingBagCubit>().getProductsShoppingCart();
        }
        if (state is ComponentsShoppingBagSuccessState) {
          return Padding(
            padding: EdgeInsets.only(top: Paddings.P_14),
            child: ListView.builder(
              itemCount: state.response.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: EdgeInsets.only(bottom: Paddings.P_14, top: Paddings.P_14),
                  child: Container(
                    decoration: BoxDecoration(
                        color: AppColors.WHITE,
                        borderRadius: BorderRadius.circular(AppRadius.RADIUS_10)),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ComponentCharacteristicField(
                              label: 'Produs',
                              data: state.response[index].title,
                            ),
                            ComponentCharacteristicField(
                              label: 'Cantitate',
                              data: state.response[index].quantity,
                            ),
                            ComponentCharacteristicField(
                              label: 'Pret Unitar',
                              data: state.response[index].price,
                            ),
                            ComponentCharacteristicField(
                              label: 'Total',
                              data: state.response[index].price! * state.response[index].quantity!,
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: Paddings.P_32),
                          child: IconButton(
                            icon: SvgPicture.asset(Assets.removeIcon),
                            onPressed: () {
                              context
                                  .read<ComponentsShoppingBagCubit>()
                                  .removeDataFromCart(state.response, state.response[index]);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          );
        } else if (state is ComponentsShoppingBagEmptyListState) {
          return const Center(child: Text('Your shopping bag is empty'));
        } else {
          return const Center(child: Text('Failed to get Data'));
        }
      },
    );
  }
}
