import 'package:flutter/material.dart';
import 'package:self_diagnosis_app/specs/colors.dart';
import 'package:self_diagnosis_app/specs/constraints.dart';

class StepContainer extends StatelessWidget {
  final int stepNumber;
  final String stepMessage;
  final Color stepColor;
  const StepContainer(
      {required this.stepNumber, required this.stepMessage, required this.stepColor, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: stepMessage.isNotEmpty ? Paddings.P_16 : 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: MediaQuery.of(context).size.height * .06,
            width: MediaQuery.of(context).size.width * .12,
            decoration: BoxDecoration(
              color: stepColor,
              shape: BoxShape.circle,
            ),
            child: Center(
                child: Text(
              '$stepNumber',
              style: const TextStyle(color: AppColors.WHITE),
            )),
          ),
          SizedBox(
            width: Heights.H_50,
            child: Text(
              stepMessage,
              textAlign: TextAlign.center,
              maxLines: 2,
            ),
          ),
        ],
      ),
    );
  }
}
