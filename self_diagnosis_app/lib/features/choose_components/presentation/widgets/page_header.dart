import 'package:flutter/material.dart';
import 'package:self_diagnosis_app/features/choose_components/presentation/widgets/step_container.dart';
import 'package:self_diagnosis_app/specs/colors.dart';
import 'package:self_diagnosis_app/specs/constraints.dart';

class PageHeader extends StatelessWidget {
  final bool step1;
  const PageHeader({required this.step1, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: EdgeInsets.only(left: Paddings.P_22, top: Paddings.P_14),
          child: StepContainer(
              stepNumber: 1,
              stepMessage: step1 ? 'Creaza deviz' : '',
              stepColor: step1 ? AppColors.DARK_RED : AppColors.LIME_GREEN),
        ),
        Center(
          child: Container(
            height: 1.0,
            width: MediaQuery.of(context).size.width * .4,
            color: AppColors.LIGHT_GREY,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(right: Paddings.P_22, top: Paddings.P_14),
          child: StepContainer(
              stepNumber: 2,
              stepMessage: !step1 ? 'Stabiliti ora' : '',
              stepColor: step1 ? AppColors.LIGHT_GREY : AppColors.DARK_RED),
        ),
      ],
    );
  }
}
