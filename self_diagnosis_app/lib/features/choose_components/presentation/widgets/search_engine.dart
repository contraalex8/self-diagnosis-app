import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:self_diagnosis_app/core/common_widgets/buttons/circular_button.dart';
import 'package:self_diagnosis_app/core/common_widgets/text_fields/regular_text_field.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';
import 'package:self_diagnosis_app/features/choose_components/presentation/bloc/components_shopping_bag_bloc/components_shopping_bag_bloc.dart';
import 'package:self_diagnosis_app/features/choose_components/presentation/bloc/get_all_products_bloc/get_all_products_bloc.dart';
import 'package:self_diagnosis_app/specs/assets.dart';
import 'package:self_diagnosis_app/specs/constraints.dart';

import '../../../../core/dependency_injection/injection_container.dart';

class SearchEngineProvider extends StatelessWidget {
  const SearchEngineProvider({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<GetProductsCubit>(
          create: (_) => injector<GetProductsCubit>(),
        ),
        BlocProvider<ComponentsShoppingBagCubit>(
          create: (_) => injector<ComponentsShoppingBagCubit>(),
        ),
      ],
      child: const SearchEngine(),
    );
  }
}

class SearchEngine extends StatefulWidget {
  const SearchEngine({Key? key}) : super(key: key);

  @override
  State<SearchEngine> createState() => _SearchEngineState();
}

class _SearchEngineState extends State<SearchEngine> {
  final _formKey = GlobalKey<FormBuilderState>();
  String currentProduct = 'empty-product';
  ProductModel currentProductModel = ProductModel();
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GetProductsCubit, GetProductsState>(builder: (context, state) {
      ///Get data from response if state is [GetProductsStateInitialState]
      if (state is GetProductsStateInitialState) {
        ///Call of the "API"
        context.read<GetProductsCubit>().getAllProducts();
      }

      ///Loading UI if state is [GetProductsStateLoadingState]
      if (state is GetProductsStateLoadingState) {
        return const Center(
          child: CircularProgressIndicator(),
        );

        /// Render UI if state is [GetProductsStateSuccessState]
      } else if (state is GetProductsStateSuccessState) {
        return FormBuilder(
          autoFocusOnValidationFailure: true,
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: Paddings.P_22, top: Paddings.P_8),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * .4,
                  child: RegularTextField(
                      onChanged: (input) {
                        String check = input ?? '';
                        setState(() {
                          currentProduct = context
                              .read<GetProductsCubit>()
                              .checkIfProductExists(state.response, check);
                          if (currentProduct != 'empty-product') {
                            currentProductModel = context
                                .read<GetProductsCubit>()
                                .getProduct(state.response, currentProduct);
                          }
                        });
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(),
                        FormBuilderValidators.equal(currentProduct, errorText: 'Produs inexistent'),
                      ]),
                      fieldName: 'search',
                      label: 'Cautare',
                      hint: 'Telescop'),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: Paddings.P_14, top: Paddings.P_8),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * .2,
                  child: RegularTextField(
                      validator: FormBuilderValidators.required(errorText: 'Cantitate?'),
                      fieldName: 'quantity',
                      label: '',
                      hint: '1'),
                ),
              ),
              BlocBuilder<ComponentsShoppingBagCubit, ComponentsShoppingBagState>(
                  builder: (context, state) {
                return Padding(
                  padding: EdgeInsets.only(left: Paddings.P_16, top: Paddings.P_8),
                  child: Center(
                      child: CircularButton(
                          onPressed: () async {
                            if (_formKey.currentState!.saveAndValidate()) {
                              int quantity =
                                  int.parse(_formKey.currentState?.fields['quantity']?.value);
                              while (quantity != 0) {
                                context
                                    .read<ComponentsShoppingBagCubit>()
                                    .saveProduct(currentProductModel);
                                quantity--;
                              }
                              _formKey.currentState?.patchValue({'search': ''});
                              _formKey.currentState?.patchValue({'quantity': ''});
                            }
                          },
                          asset: Assets.addIcon)),
                );
              }),
            ],
          ),
        );
      } else {
        return const Center(
          child: Text('Failed to fetch data!'),
        );
      }
    });
  }
}
