import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';

abstract class GetProductsDataSource {
  Future<List<ProductModel>> getListOfProducts();
}
