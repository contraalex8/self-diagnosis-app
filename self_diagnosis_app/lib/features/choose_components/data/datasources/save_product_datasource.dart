import '../models/product_model.dart';

abstract class SaveProductDataSource {
  Future<bool> saveProductInLocalStorage(ProductModel productModel);
}
