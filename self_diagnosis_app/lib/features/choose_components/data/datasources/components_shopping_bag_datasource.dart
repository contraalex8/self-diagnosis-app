import 'package:self_diagnosis_app/features/choose_components/data/models/component_in_bag.dart';

abstract class ComponentsShoppingBagDataSource {
  Future<List<ComponentInBagModel>> getBagContent();
  Future<List<ComponentInBagModel>> removeBagContent(
      List<ComponentInBagModel> currentList, ComponentInBagModel product);
}
