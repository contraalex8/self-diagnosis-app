import 'package:self_diagnosis_app/features/choose_components/data/datasources/products_datasource.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';

class GetProductsDataSourceImpl extends GetProductsDataSource {
  ///[getListOfProducts] -> mocks a Restful API call to get the existing components
  @override
  Future<List<ProductModel>> getListOfProducts() async {
    return [
      ProductModel(
        title: 'Toba esapament',
        price: 899,
      ),
      ProductModel(
        title: 'Distributie',
        price: 799,
      ),
      ProductModel(
        title: 'Motor',
        price: 499,
      ),
      ProductModel(
        title: 'Blindaj fata',
        price: 2600,
      ),
    ];
  }
}
