import 'package:self_diagnosis_app/features/choose_components/data/models/component_in_bag.dart';

import '../../../../core/dependency_injection/injection_module.dart';
import '../../../../core/shared_preferences/shared_preferences.dart';
import '../../../../core/shared_preferences/shared_preferences_keys.dart';
import '../models/product_model.dart';
import 'components_shopping_bag_datasource.dart';

class ComponentsShoppingBagDataSourceImpl implements ComponentsShoppingBagDataSource {
  @override
  Future<List<ComponentInBagModel>> getBagContent() async {
    ///Get the  list in [SharedPreferencesKeys.shipComponents] as a List<ComponentModel>
    return _getRepairComponents(await _getCurrentListOfProducts());
  }

  @override
  Future<List<ComponentInBagModel>> removeBagContent(
      List<ComponentInBagModel> currentList, ComponentInBagModel product) async {
    if (currentList.isNotEmpty) {
      currentList.remove(product);
      String convertedList = ProductModel.encode(getListOfProductsAfterRemove(currentList));
      InjectionModule.injector<SharedPreferencesAbstract>()
          .setString(SharedPreferencesKeys.shipComponents, convertedList);
    }
    return currentList.isEmpty ? [] : currentList;
  }

  Future<List<ProductModel>> _getCurrentListOfProducts() async {
    ///Get the list already store [currentList]
    String currentList = InjectionModule.injector<SharedPreferencesAbstract>()
            .getString(SharedPreferencesKeys.shipComponents) ??
        '';

    return currentList.isEmpty ? [] : ProductModel.decode(currentList);
  }

  ///[_getRepairComponents] constructing the bag models
  List<ComponentInBagModel> _getRepairComponents(List<ProductModel> products) {
    if (products.isNotEmpty) {
      Map<ProductModel, int> productCount = getMappedProducts(products);
      List<ComponentInBagModel> repairComponents = [];

      ///construct [repairComponents]
      productCount.forEach((ProductModel product, int count) {
        ComponentInBagModel repairComponent =
            ComponentInBagModel(title: product.title, price: product.price, quantity: count);
        repairComponents.add(repairComponent);
      });
      return repairComponents;
    }

    return [];
  }

  ///[getMappedProducts] mapping the current list of selected products
  Map<ProductModel, int> getMappedProducts(List<ProductModel> products) {
    Map<ProductModel, int> productCount = {};
    for (ProductModel product in products) {
      if (productCount.containsKey(product)) {
        productCount[product] = productCount[product]! + 1;
      } else {
        productCount[product] = 1;
      }
    }
    return productCount;
  }

  ///[getListOfProductsAfterRemove] is preparing the new list of products to be set in [SharedPreferences]
  List<ProductModel> getListOfProductsAfterRemove(List<ComponentInBagModel> products) {
    List<ProductModel> currentList = [];
    for (var product in products) {
      int quantity = product.quantity!;
      while (quantity != 0) {
        currentList.add(ProductModel(title: product.title, price: product.price));
        quantity--;
      }
    }
    return currentList;
  }
}
