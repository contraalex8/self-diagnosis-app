import 'package:self_diagnosis_app/features/choose_components/data/datasources/save_product_datasource.dart';

import '../../../../core/dependency_injection/injection_module.dart';
import '../../../../core/shared_preferences/shared_preferences.dart';
import '../../../../core/shared_preferences/shared_preferences_keys.dart';
import '../models/product_model.dart';

class SaveProductDataSourceImpl implements SaveProductDataSource {
  @override
  Future<bool> saveProductInLocalStorage(ProductModel productModel) async {
    ///Get the list already stored [currentList]
    String currentList = InjectionModule.injector<SharedPreferencesAbstract>()
            .getString(SharedPreferencesKeys.shipComponents) ??
        '';
    late String newList = '';

    ///setup new list
    if (currentList.isNotEmpty) {
      List<ProductModel> decodedList = ProductModel.decode(currentList);
      decodedList.add(productModel);
      newList = ProductModel.encode(decodedList);
    } else {
      String newProduct = ProductModel.encode([productModel]);
      newList += newProduct;
    }

    ///Save the new list in [SharedPreferencesKeys.shipComponents]
    return await InjectionModule.injector<SharedPreferencesAbstract>()
        .setString(SharedPreferencesKeys.shipComponents, newList);
  }
}
