// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'component_in_bag.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ComponentInBagModel _$ComponentInBagModelFromJson(Map<String, dynamic> json) {
  return _ComponentInBagModel.fromJson(json);
}

/// @nodoc
mixin _$ComponentInBagModel {
  String? get title => throw _privateConstructorUsedError;
  double? get price => throw _privateConstructorUsedError;
  int? get quantity => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ComponentInBagModelCopyWith<ComponentInBagModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ComponentInBagModelCopyWith<$Res> {
  factory $ComponentInBagModelCopyWith(
          ComponentInBagModel value, $Res Function(ComponentInBagModel) then) =
      _$ComponentInBagModelCopyWithImpl<$Res, ComponentInBagModel>;
  @useResult
  $Res call({String? title, double? price, int? quantity});
}

/// @nodoc
class _$ComponentInBagModelCopyWithImpl<$Res, $Val extends ComponentInBagModel>
    implements $ComponentInBagModelCopyWith<$Res> {
  _$ComponentInBagModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = freezed,
    Object? price = freezed,
    Object? quantity = freezed,
  }) {
    return _then(_value.copyWith(
      title: freezed == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double?,
      quantity: freezed == quantity
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ComponentInBagModelCopyWith<$Res>
    implements $ComponentInBagModelCopyWith<$Res> {
  factory _$$_ComponentInBagModelCopyWith(_$_ComponentInBagModel value,
          $Res Function(_$_ComponentInBagModel) then) =
      __$$_ComponentInBagModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? title, double? price, int? quantity});
}

/// @nodoc
class __$$_ComponentInBagModelCopyWithImpl<$Res>
    extends _$ComponentInBagModelCopyWithImpl<$Res, _$_ComponentInBagModel>
    implements _$$_ComponentInBagModelCopyWith<$Res> {
  __$$_ComponentInBagModelCopyWithImpl(_$_ComponentInBagModel _value,
      $Res Function(_$_ComponentInBagModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = freezed,
    Object? price = freezed,
    Object? quantity = freezed,
  }) {
    return _then(_$_ComponentInBagModel(
      title: freezed == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      price: freezed == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double?,
      quantity: freezed == quantity
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ComponentInBagModel implements _ComponentInBagModel {
  _$_ComponentInBagModel({this.title, this.price, this.quantity});

  factory _$_ComponentInBagModel.fromJson(Map<String, dynamic> json) =>
      _$$_ComponentInBagModelFromJson(json);

  @override
  final String? title;
  @override
  final double? price;
  @override
  final int? quantity;

  @override
  String toString() {
    return 'ComponentInBagModel(title: $title, price: $price, quantity: $quantity)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ComponentInBagModel &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.price, price) || other.price == price) &&
            (identical(other.quantity, quantity) ||
                other.quantity == quantity));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, title, price, quantity);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ComponentInBagModelCopyWith<_$_ComponentInBagModel> get copyWith =>
      __$$_ComponentInBagModelCopyWithImpl<_$_ComponentInBagModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ComponentInBagModelToJson(
      this,
    );
  }
}

abstract class _ComponentInBagModel implements ComponentInBagModel {
  factory _ComponentInBagModel(
      {final String? title,
      final double? price,
      final int? quantity}) = _$_ComponentInBagModel;

  factory _ComponentInBagModel.fromJson(Map<String, dynamic> json) =
      _$_ComponentInBagModel.fromJson;

  @override
  String? get title;
  @override
  double? get price;
  @override
  int? get quantity;
  @override
  @JsonKey(ignore: true)
  _$$_ComponentInBagModelCopyWith<_$_ComponentInBagModel> get copyWith =>
      throw _privateConstructorUsedError;
}
