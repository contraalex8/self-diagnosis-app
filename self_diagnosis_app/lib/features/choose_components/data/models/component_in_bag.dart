import 'dart:convert';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'component_in_bag.freezed.dart';
part 'component_in_bag.g.dart';

@freezed
class ComponentInBagModel with _$ComponentInBagModel {
  factory ComponentInBagModel({
    String? title,
    double? price,
    int? quantity,
  }) = _ComponentInBagModel;

  factory ComponentInBagModel.fromJson(Map<String, dynamic> json) =>
      _$ComponentInBagModelFromJson(json);

  static Map<String, dynamic> toMap(ComponentInBagModel product) =>
      {'title': product.title, 'price': product.price, 'quantity': product.quantity};
  static String encode(List<ComponentInBagModel> products) => json.encode(
        products
            .map<Map<String, dynamic>>((products) => ComponentInBagModel.toMap(products))
            .toList(),
      );

  static List<ComponentInBagModel> decode(String products) =>
      (json.decode(products) as List<dynamic>)
          .map<ComponentInBagModel>((item) => ComponentInBagModel.fromJson(item))
          .toList();
}
