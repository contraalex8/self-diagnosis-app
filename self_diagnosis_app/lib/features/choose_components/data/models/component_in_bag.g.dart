// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'component_in_bag.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ComponentInBagModel _$$_ComponentInBagModelFromJson(
        Map<String, dynamic> json) =>
    _$_ComponentInBagModel(
      title: json['title'] as String?,
      price: (json['price'] as num?)?.toDouble(),
      quantity: json['quantity'] as int?,
    );

Map<String, dynamic> _$$_ComponentInBagModelToJson(
        _$_ComponentInBagModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'price': instance.price,
      'quantity': instance.quantity,
    };
