import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/core/error/failures.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';

import '../../domain/repositories/get_list_of_products_repository.dart';
import '../datasources/products_datasource.dart';

class GetListOfProductsRepositoryImpl implements GetListOfProductsRepository {
  final GetProductsDataSource _getProducts;
  GetListOfProductsRepositoryImpl({
    required GetProductsDataSource getProducts,
  }) : _getProducts = getProducts;

  @override
  Future<Either<Failure, List<ProductModel>>> getProductList() async {
    try {
      final getProductsList = await _getProducts.getListOfProducts();
      return Right(getProductsList);
    } catch (e) {
      return const Left(StandardFailure('Standard failure error'));
    }
  }
}
