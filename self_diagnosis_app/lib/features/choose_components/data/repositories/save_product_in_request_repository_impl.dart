import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/core/error/failures.dart';
import 'package:self_diagnosis_app/features/choose_components/data/datasources/save_product_datasource.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/product_model.dart';

import '../../domain/repositories/save_product_in_request_repository.dart';

class SaveProductRepositoryImpl implements SaveProductRepository {
  final SaveProductDataSource _saveProductDataSource;
  SaveProductRepositoryImpl({
    required SaveProductDataSource saveProductDataSource,
  }) : _saveProductDataSource = saveProductDataSource;

  @override
  Future<Either<Failure, bool>> saveProduct(ProductModel product) async {
    try {
      final bool saveProduct = await _saveProductDataSource.saveProductInLocalStorage(product);
      return Right(saveProduct);
    } catch (e) {
      return const Left(StandardFailure('Product is not saved'));
    }
  }
}
