import 'package:dartz/dartz.dart';
import 'package:self_diagnosis_app/core/error/failures.dart';
import 'package:self_diagnosis_app/features/choose_components/data/models/component_in_bag.dart';

import '../../domain/repositories/components_shopping_bag_repository.dart';
import '../datasources/components_shopping_bag_datasource.dart';

class ComponentsShoppingBagRepositoryImpl implements ComponentsShoppingBagRepository {
  final ComponentsShoppingBagDataSource _componentsShoppingBagDataSource;
  ComponentsShoppingBagRepositoryImpl({
    required ComponentsShoppingBagDataSource componentsShoppingBagDataSource,
  }) : _componentsShoppingBagDataSource = componentsShoppingBagDataSource;
  @override
  Future<Either<Failure, List<ComponentInBagModel>>> getBagContent() async {
    try {
      final products = await _componentsShoppingBagDataSource.getBagContent();
      return Right(products);
    } catch (e) {
      return const Left(StandardFailure('Failed to get bag content'));
    }
  }

  @override
  Future<Either<Failure, List<ComponentInBagModel>>> removeBagContent(
      List<ComponentInBagModel> currentList, ComponentInBagModel product) async {
    try {
      final productsToRemove =
          await _componentsShoppingBagDataSource.removeBagContent(currentList, product);
      return Right(productsToRemove);
    } catch (e) {
      return const Left(StandardFailure('Failed to remove product'));
    }
  }
}
