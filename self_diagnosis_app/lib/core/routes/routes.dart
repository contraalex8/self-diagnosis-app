import 'package:flutter/material.dart';
import 'package:self_diagnosis_app/features/select_date/presentation/pages/select_date_page_ui.dart';

import '../../features/choose_components/presentation/pages/choose_components_view_ui.dart';
import '../../features/select_repair_station/presentation/pages/select_repair_station_page_ui.dart';

class Navigators {
  ///[getToStage1Route] routes to the first page [select_components]
  static getToStage1Route(BuildContext context) =>
      Navigator.pop(context, MaterialPageRoute(builder: (context) => const ChooseComponentsView()));

  ///[getToStage2Route] routes to the second page [calendar]
  static getToStage2Route(BuildContext context) =>
      Navigator.push(context, MaterialPageRoute(builder: (context) => const SelectDatePageView()));

  ///[getToStage3Route] routes to the second page [select_repair_station]
  static getToStage3Route(BuildContext context) => Navigator.push(
      context, MaterialPageRoute(builder: (context) => const SelectRepairStationView()));
}
