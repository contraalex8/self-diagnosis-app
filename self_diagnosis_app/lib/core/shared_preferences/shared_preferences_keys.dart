/// Constants for SharedPreferences keys
class SharedPreferencesKeys {
  /// [shipComponents] -> access key
  static const shipComponents = 'shipComponents';

  /// [appointmentDate] -> access key
  static const appointmentDate = 'appointmentDate';
}
