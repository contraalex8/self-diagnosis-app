import 'dart:async';

import 'package:get_it/get_it.dart';
import 'package:self_diagnosis_app/features/choose_components/data/datasources/components_shopping_bag_datasource.dart';
import 'package:self_diagnosis_app/features/choose_components/data/datasources/products_datasource_impl.dart';
import 'package:self_diagnosis_app/features/choose_components/data/datasources/save_product_datasource.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/repositories/components_shopping_bag_repository.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/repositories/save_product_in_request_repository.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/usecases/components_shopping_bag_usecase.dart';
import 'package:self_diagnosis_app/features/choose_components/domain/usecases/save_product_in_request_usecase.dart';
import 'package:self_diagnosis_app/features/choose_components/presentation/bloc/components_shopping_bag_bloc/components_shopping_bag_bloc.dart';
import 'package:self_diagnosis_app/features/select_date/data/datasources/save_date_of_appointment_datasource.dart';
import 'package:self_diagnosis_app/features/select_date/domain/repositories/save_date_of_appointment_repository.dart';
import 'package:self_diagnosis_app/features/select_date/domain/usecases/save_date_of_appointment_usecase.dart';
import 'package:self_diagnosis_app/features/select_date/presentation/bloc/save_date_of_appointment_bloc.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/datasources/get_appointment_date_datasource.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/datasources/repair_stations_datasource.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/datasources/sort_repair_stations_datasource.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/datasources/sort_repair_stations_datasource_impl.dart';
import 'package:self_diagnosis_app/features/select_repair_station/data/repositories/sort_repair_stations_repository_impl.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/repositories/get_appointment_date_repository.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/repositories/repair_stations_repository.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/repositories/sort_repair_stations_repository.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/usecases/get_appointment_date_usecase.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/usecases/repair_stations_usecase.dart';
import 'package:self_diagnosis_app/features/select_repair_station/domain/usecases/sort_repair_stations_usecase.dart';
import 'package:self_diagnosis_app/features/select_repair_station/presentation/bloc/get_appointment_date_bloc/get_appointment_date_bloc.dart';
import 'package:self_diagnosis_app/features/select_repair_station/presentation/bloc/get_repair_stations_bloc/get_repair_stations_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../features/choose_components/data/datasources/components_shopping_bag_datasource_impl.dart';
import '../../features/choose_components/data/datasources/products_datasource.dart';
import '../../features/choose_components/data/datasources/save_product_datasource_impl.dart';
import '../../features/choose_components/data/repositories/components_shopping_bag_repository_impl.dart';
import '../../features/choose_components/data/repositories/get_list_of_products_repository_impl.dart';
import '../../features/choose_components/data/repositories/save_product_in_request_repository_impl.dart';
import '../../features/choose_components/domain/repositories/get_list_of_products_repository.dart';
import '../../features/choose_components/domain/usecases/get_list_of_products_usecase.dart';
import '../../features/choose_components/presentation/bloc/get_all_products_bloc/get_all_products_bloc.dart';
import '../../features/select_date/data/datasources/save_date_of_appointment_datasource_impl.dart';
import '../../features/select_date/data/repositories/save_date_of_appointment_repository_impl.dart';
import '../../features/select_repair_station/data/datasources/get_appointment_date_datasource_impl.dart';
import '../../features/select_repair_station/data/datasources/repair_stations_datsource_impl.dart';
import '../../features/select_repair_station/data/repositories/get_appointment_date_repository_impl.dart';
import '../../features/select_repair_station/data/repositories/repair_stations_repository_impl.dart';
import '../shared_preferences/shared_preferences.dart';
import '../shared_preferences/shared_preferences_impl.dart';

final injector = GetIt.instance;

FutureOr<void> initContainer() async {
  await _initDependencies();
}

FutureOr<void> _initDependencies() async {
  ///Other
  /// SharedPreferences dependencies
  final sharedPreferences = await SharedPreferences.getInstance();

  injector.registerLazySingleton<SharedPreferencesAbstract>(
      () => SharedPreferencesImpl(sharedPreferences: sharedPreferences));

  ///Blocs
  injector.registerLazySingleton<GetProductsCubit>(
      () => GetProductsCubit(getListOfProductsUseCase: injector()));

  injector.registerLazySingleton<ComponentsShoppingBagCubit>(() => ComponentsShoppingBagCubit(
        componentsShoppingBagUseCase: injector(),
        saveProductInRequestUseCase: injector(),
      ));

  injector.registerLazySingleton<SaveDateAppointmentCubit>(
      () => SaveDateAppointmentCubit(saveDateAppointmentUseCase: injector()));

  injector.registerLazySingleton<GetAppointmentDateCubit>(
      () => GetAppointmentDateCubit(getAppointmentDateUseCase: injector()));

  injector.registerLazySingleton<GetRepairStationsCubit>(() => GetRepairStationsCubit(
        getRepairStationsUseCase: injector(),
        sortRepairStationsUseCase: injector(),
      ));

  ///Usecases
  injector
      .registerLazySingleton<GetListOfProductsUseCase>(() => GetListOfProductsUseCase(injector()));

  injector.registerLazySingleton<SaveProductInRequestUseCase>(
      () => SaveProductInRequestUseCase(injector()));

  injector.registerLazySingleton<ComponentsShoppingBagUseCase>(
      () => ComponentsShoppingBagUseCase(injector()));

  injector.registerLazySingleton<SaveDateAppointmentUseCase>(
      () => SaveDateAppointmentUseCase(injector()));

  injector.registerLazySingleton<GetAppointmentDateUseCase>(
      () => GetAppointmentDateUseCase(injector()));

  injector
      .registerLazySingleton<GetRepairStationsUseCase>(() => GetRepairStationsUseCase(injector()));

  injector.registerLazySingleton<SortRepairStationsUseCase>(
      () => SortRepairStationsUseCase(injector()));

  ///Repositories
  injector.registerLazySingleton<GetListOfProductsRepository>(
      () => GetListOfProductsRepositoryImpl(getProducts: injector()));

  injector.registerLazySingleton<SaveProductRepository>(
      () => SaveProductRepositoryImpl(saveProductDataSource: injector()));

  injector.registerLazySingleton<ComponentsShoppingBagRepository>(
      () => ComponentsShoppingBagRepositoryImpl(componentsShoppingBagDataSource: injector()));

  injector.registerLazySingleton<SaveDateAppointmentRepository>(
      () => SaveDateAppointmentRepositoryImpl(saveDateAppointmentDataSource: injector()));

  injector.registerLazySingleton<GetAppointmentDateRepository>(
      () => GetAppointmentDateRepositoryImpl(getAppointmentDateDataSource: injector()));

  injector.registerLazySingleton<GetRepairStationsRepository>(
      () => GetRepairStationsRepositoryImpl(repairStationsDataSource: injector()));

  injector.registerLazySingleton<SortRepairStationsRepository>(
      () => SortRepairStationsRepositoryImpl(sortRepairStationsDataSource: injector()));

  ///Datasources
  injector.registerLazySingleton<GetProductsDataSource>(() => GetProductsDataSourceImpl());

  injector.registerLazySingleton<SaveProductDataSource>(() => SaveProductDataSourceImpl());

  injector.registerLazySingleton<ComponentsShoppingBagDataSource>(
      () => ComponentsShoppingBagDataSourceImpl());

  injector.registerLazySingleton<SaveDateAppointmentDataSource>(
      () => SaveDateAppointmentDataSourceImpl());

  injector.registerLazySingleton<GetAppointmentDateDataSource>(
      () => GetAppointmentDateDataSourceImpl());

  injector.registerLazySingleton<RepairStationsDataSource>(() => RepairStationsDataSourceImpl());

  injector.registerLazySingleton<SortRepairStationsDataSource>(
      () => SortRepairStationsDataSourceImpl());
}
