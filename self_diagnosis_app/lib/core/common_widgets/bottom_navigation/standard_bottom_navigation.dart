import 'package:flutter/material.dart';
import 'package:self_diagnosis_app/specs/colors.dart';
import 'package:self_diagnosis_app/specs/constraints.dart';
import 'package:self_diagnosis_app/specs/text_sizes.dart';

class StandardBottomNavigation extends StatelessWidget {
  final bool isSecondRoute;
  final void Function() onPressedBack;
  final void Function() onPressedNext;
  const StandardBottomNavigation(
      {required this.isSecondRoute,
      required this.onPressedBack,
      required this.onPressedNext,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: Heights.H_100,
      child: BottomAppBar(
        elevation: 0,
        color: AppColors.WHITE,
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Visibility(
                visible: isSecondRoute,
                child: Padding(
                  padding: EdgeInsets.only(left: Paddings.P_22),
                  child: TextButton(
                    onPressed: onPressedBack,
                    child: Text(
                      'Inapoi',
                      style: TextStyle(
                        color: AppColors.LIGHT_GREY,
                        fontSize: TextSizes.SIZE_20,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: Paddings.P_22),
                child: TextButton(
                  onPressed: onPressedNext,
                  child: Text(
                    'Inainte',
                    style: TextStyle(
                      color: AppColors.LIGHT_GREY,
                      fontSize: TextSizes.SIZE_20,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
