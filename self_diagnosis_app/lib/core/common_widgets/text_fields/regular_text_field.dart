import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:self_diagnosis_app/core/common_widgets/text_fields/text_styles.dart';

class RegularTextField extends StatelessWidget {
  final void Function(String?)? onSubmitted;
  final void Function(String?)? onChanged;
  final String? Function(String?) validator;
  final TextEditingController? controller;
  final String fieldName;
  final String label;
  final String hint;
  final bool isObscure;
  const RegularTextField({
    required this.validator,
    required this.fieldName,
    required this.label,
    required this.hint,
    this.onSubmitted,
    this.onChanged,
    this.isObscure = false,
    this.controller,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      name: fieldName,
      onChanged: onChanged,
      obscureText: isObscure,
      controller: controller ?? controller,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onSubmitted: onSubmitted,
      textInputAction: TextInputAction.done,
      style: Theme.of(context).textTheme.titleSmall,
      validator: validator,
      decoration: TextStyles().textFieldStandardDecoration(label, hint, const Text(''), context),
    );
  }
}
