import 'package:flutter/material.dart';

import '../../../specs/colors.dart';
import '../../../specs/radiuses.dart';

class TextStyles {
  static ButtonStyle setButtonStandardStyle(Color buttonColor) {
    return ButtonStyle(
      backgroundColor: MaterialStateProperty.all<Color>(buttonColor),
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(AppRadius.RADIUS_10)),
      )),
    );
  }

  InputDecoration textFieldStandardDecoration(
      String label, String hint, Widget suffixIcon, BuildContext context) {
    return InputDecoration(
      enabledBorder: outlineInputBorder(AppColors.LIGHT_GREY),
      errorBorder: outlineInputBorder(AppColors.DARK_RED),
      focusedErrorBorder: outlineInputBorder(AppColors.DARK_RED),
      disabledBorder: outlineInputBorder(AppColors.LIGHT_GREY),
      focusedBorder: outlineInputBorder(AppColors.DARK_GREEN),
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(AppRadius.RADIUS_3)),
      filled: true,
      focusColor: Theme.of(context).inputDecorationTheme.focusColor,
      fillColor: Theme.of(context).inputDecorationTheme.fillColor,
      suffixIcon: suffixIcon,
      label: Text(label),
      labelStyle: Theme.of(context).inputDecorationTheme.labelStyle,
      hintText: hint,
    );
  }

  static OutlineInputBorder outlineInputBorder(Color color) {
    return OutlineInputBorder(
        borderSide: BorderSide(
      color: color,
      width: 2.0,
    ));
  }
}
