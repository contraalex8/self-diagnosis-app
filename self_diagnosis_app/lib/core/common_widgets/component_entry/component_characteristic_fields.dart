import 'package:flutter/material.dart';
import 'package:self_diagnosis_app/specs/colors.dart';
import 'package:self_diagnosis_app/specs/constraints.dart';
import 'package:self_diagnosis_app/specs/text_sizes.dart';

class ComponentCharacteristicField extends StatelessWidget {
  final String label;
  final dynamic data;
  const ComponentCharacteristicField({required this.label, required this.data, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(left: Paddings.P_22, bottom: Paddings.P_6, right: Paddings.P_16),
          child: Text(
            label,
            style: TextStyle(
              color: AppColors.LIGHT_GREY,
              fontSize: TextSizes.SIZE_16,
            ),
          ),
        ),
        Text(
          '$data',
          style: TextStyle(
            color: AppColors.DARK_GREEN,
            fontSize: TextSizes.SIZE_16,
          ),
        ),
      ],
    );
  }
}
