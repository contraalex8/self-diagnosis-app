import 'package:flutter/material.dart';

class StandardButton extends StatelessWidget {
  final void Function() onPressed;

  const StandardButton({required this.onPressed, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(shape: BoxShape.rectangle),
      child: ElevatedButton(
        onPressed: onPressed,
        child: const Text('Alege data'),
      ),
    );
  }
}
