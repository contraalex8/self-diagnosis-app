import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:self_diagnosis_app/specs/colors.dart';

class CircularButton extends StatelessWidget {
  final void Function() onPressed;
  final String asset;
  const CircularButton({required this.onPressed, required this.asset, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(shape: BoxShape.circle),
      child: FloatingActionButton(
        onPressed: onPressed,
        backgroundColor: AppColors.DARK_RED,
        child: SvgPicture.asset(asset, color: AppColors.WHITE),
      ),
    );
  }
}
