// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:self_diagnosis_app/specs/colors.dart';

import '../../../specs/constraints.dart';

class StandardAppBar extends StatefulWidget implements PreferredSizeWidget {
  final double appBarHeight = Heights.H_27;
  StandardAppBar({Key? key}) : super(key: key);

  @override
  State<StandardAppBar> createState() => _StandardAppBarState();

  @override
  Size get preferredSize => Size(double.infinity, appBarHeight);
}

class _StandardAppBarState extends State<StandardAppBar> {
  @override
  Widget build(BuildContext context) {
    return MediaQuery(
        data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
        child: AppBar(
            automaticallyImplyLeading: false, backgroundColor: AppColors.WHITE, elevation: 0));
  }
}
