import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  ///[message] -> in failure
  final String message;

  ///Default [Failure] constructor
  const Failure(this.message);

  @override
  List<Object> get props => [message];
}

class StandardFailure extends Failure {
  /// Default [StandardFailure] constructor
  const StandardFailure(String message) : super(message);
}
